<?php

function filter_xml($matches) { 
    return trim(htmlspecialchars($matches[1])); 
} 
$url = "http://www.tbs.com/video/content/services/getCollections.do?id=185669";
$article_string = file_get_contents($url); 
$article_string = preg_replace_callback('/<!\[CDATA\[(.*)\]\]>/', 'filter_xml', $article_string); 
$article_xml = simplexml_load_string($article_string);  
header('Content-type: application/json');
print json_encode($article_xml);

?>