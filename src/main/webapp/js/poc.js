// We will need to set up a configuration of what is needed per page
var pageMods = {};

/*

example of extending slider
slider.setOpts = function(opts) {
  for (var opt in opts) {
    vars[opt] = opts[opt];
  }
  slider.setup();
}

slider.getOpts = function() {
  return vars;
}

example of removing and readding slider with new opts
http://2002-2012.mattwilcox.net/archive/entry/id/1088/
*/

(function(window, $, undefined) {

	var supportsOrientationChange = "onorientationchange" in window,
		orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";
	
	// we want to know if the device is mobile, full episodes cannot play on mobile
	// we may need to beef this up, but for now desktop does not support orientation
	var isMobile = typeof window.orientation !== 'undefined' ? true : false;


	window.addEventListener(orientationEvent, function() {
		resetFlexSlider();
	}, false);
	
	// note these are not optimized for mobile yet
	var windowHt = $(window).height();
	var windowW = $(window).width();
	//var mlSlideItemWidth = windowW <= 480 ? 480 : 356;
	var slideItemWidth;
	
	// the +10 is to take the li margin into account
	
	function getSlideItemW(){
		//console.log("getSlideItemW");
		if($("#page-livetv").length > 0){
			return (($("#page-livetv section.slider").innerWidth()) / 2) -7;
		} else if($("#page-schedule").length > 0 || $("#page-shows").length > 0){
			return (parseInt( $(window).width() / 3) - 10); 
		} else {
			return 356;
		}
	}
	//131 + 265 = 396
	
	
	/*
		TODO: set up infrastructure to manage multiple flexsliders
	*/
	
	var sliders = [];
	
	$(document).ready(function(){
			
		//$("header .navbar-collapse").css("height",windowHt);
	
		//define some events for the POC - demo purposes only
		
		$(".signin-wrapper .logged-out a").on("click", function(){
			event.preventDefault();
			$(".signin-wrapper .logged-out").hide();
			$(".signin-wrapper .logged-in").show();
		});
		$(".signin-wrapper .logged-in").on("click", function(){
			$(this).hide();
			$(".signin-wrapper .logged-out").show();
		});
		$("button.navbar-toggle").on("click", function(){
			$("#navmain").toggle();
			//$("#navmain").css("overflow-y","auto");

		});

		
	});
	
	
	
	$(window).load(function(){
		slideItemWidth = getSlideItemW();
		console.log("slideItemWidth: " + slideItemWidth);
		$('#page-landing .flexslider').flexslider({
			animation: "slide",
			animationLoop: false,
			itemWidth: slideItemWidth,
			touch: true,
			controlNav: false,
			pausePlay: false,
			slideshow: false,
			startAt: 0,
			minItems: 0,
			itemMargin: 0,
			animationLoop: false,
			smoothHeight: true,
			move: 1,
			start: function(slider){
			  $('body').removeClass('loading');
			  sliders.push(slider);
			  console.log('slider start');
			  console.log('slider animating: ' + slider.animating);
			},
			before: function(slider){
			},
			after: function(slider){
			},
			end: function(slider){
			},
			added: function(slider){
			},
			removed: function(slider){
			}
		});
		
		$('#page-livetv .flexslider').flexslider({
			id: "livetv",
			animation: "slide",
			animationLoop: false,
			itemWidth: slideItemWidth,
			touch: true,
			controlNav: false,
			pausePlay: false,
			slideshow: false,
			startAt: 0,
			minItems: 0,
			itemMargin: 0,
			animationLoop: false,
			smoothHeight: true,
			move: 1,
			start: function(slider){
			  $('body').removeClass('loading');
			  sliders.push(slider);
			  console.log('slider start');
			  console.log('slider animating: ' + slider.animating);
			},
			before: function(slider){
			},
			after: function(slider){
			},
			end: function(slider){
			},
			added: function(slider){
			},
			removed: function(slider){
			}
		});
		
		$('#page-schedule .flexslider').flexslider({
			animation: "slide",
			animationLoop: false,
			itemWidth: slideItemWidth,
			touch: true,
			controlNav: false,
			pausePlay: false,
			slideshow: false,
			startAt: 0,
			minItems: 0,
			animationLoop: false,
			smoothHeight: true,
			move: 1,
			start: function(slider){
				$('body').removeClass('loading');
				console.log( "schedule start" );
				//setHtTimeDate("#page-schedule ul.slides li:first-child");
				setHtTimeDate("#page-schedule ul.slides li:first-child", ".dateslider .timedate");
				//console.log(slider.vars);
				sliders.push(slider);
			},
			before: function(slider){			  
			},
			after: function(slider){
			},
			end: function(slider){
			},
			added: function(slider){
			},
			removed: function(slider){
			}
		});
		$('#page-shows .flexslider').flexslider({
			animation: "slide",
			animationLoop: false,
			itemWidth: slideItemWidth,
			touch: true,
			controlNav: false,
			pausePlay: false,
			slideshow: false,
			startAt: 0,
			minItems: 0,
			animationLoop: false,
			smoothHeight: true,
			move: 1,
			start: function(slider){
				$('body').removeClass('loading');
				console.log( "schedule start" );
				//setHtTimeDate("#page-shows ul.slides li:first-child");
				setHtTimeDate("#page-shows ul.slides li:first-child", ".dateslider .showsmeta");
				//console.log(slider.vars);
				sliders.push(slider);
			},
			before: function(slider){			  
			},
			after: function(slider){
			},
			end: function(slider){
			},
			added: function(slider){
			},
			removed: function(slider){
			}
		});

	/*
		$('#page-schedule .verticalslider').flexslider({
			animation: "slide",
			animationLoop: false,
			direction: "vertical",
			itemWidth: slideItemWidth,
			touch: true,
			controlNav: false,
			pausePlay: false,
			slideshow: false,
			startAt: 0,
			minItems: 0,
			animationLoop: false,
			smoothHeight: true,
			move: 1,
			start: function(slider){
			  $('body').removeClass('loading');
			  //sliders = slider;
			},
			before: function(slider){			  
			},
			after: function(slider){
			},
			end: function(slider){
			},
			added: function(slider){
			},
			removed: function(slider){
			}
		});
		*/

	});
	
	
	
	$('.page .flexslider').on("touchstart", function(){
		console.log("touchstart");
		slideFooterControls('fadeout');
		//toggleFooter();
	});
	$('.page .flexslider').on("touchend", function(){
		console.log("touchend");
		slideFooterControls('fadein');
	});
	$('.verticalslider').on("touchstart", function(){
		console.log("touchstart");
		slideFooterControls('fadeout');
		//toggleFooter();
	});
	$('.verticalslider').on("touchend", function(){
		console.log("touchend");
		slideFooterControls('fadein');
	});
	/*
		TODO: need to know how exactly we want the footer to work on small screens
	*/
	/*
	function toggleFooter(){
		//var a = typeof(action) != 'undefined' ? action.toLowerCase() : "";
		if( windowHt < 500){
			$('footer').toggle();
		}
	}
	*/
	
	//window.addEventListener("resize", resetFlexSlider, false);
	
	function resetFlexSlider(){
		slideItemWidth = getSlideItemW();
		//console.log("reset slideItemWidth: " + slideItemWidth);
		if(sliders.length > 0){
			$(sliders).each(function(){
				var slider = this;
				//console.log(slider.vars);
				var opts = {
					itemWidth: slideItemWidth
				};
				slider.setOpts(opts);
				/* hack for schedule, we need to store functions needed at start of scroll */
				
				if( $("#page-schedule").length > 0){
					window.setTimeout(function(){
						setHtTimeDate("#page-schedule ul.slides li:first-child", ".dateslider .timedate");
					}, 200);
				} else if( $("#page-shows").length > 0){
					window.setTimeout(function(){
						setHtTimeDate("#page-shows ul.slides li:first-child", ".dateslider .showsmeta");
					}, 200);
				}
			});
		};
	}
	
	function setHtTimeDate(slidesSel, parSel){
		var slides = $(slidesSel);
		var slideHt = $(slides[0]).height();
		var par = $(parSel);
		par.css("height", slideHt);
	}
	
	function slideFooterControls(action){
		var a = typeof(action) != 'undefined' ? action.toLowerCase() : "";
		if( windowW > 345 && windowHt < 500){
			if(a == 'fadeout'){
				$('footer').hide("100");
			} else if(a == 'fadein'){
				window.setTimeout(function(){
					$('footer').fadeIn("slow", "linear");
				}, 800);
			}
		}
	}
	
	

})(window, jQuery);