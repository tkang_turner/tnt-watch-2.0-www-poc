/**
 * @class Tn.Users
 * @singleton
 * Library for dealing with users from the [Gigya platform](http://www.gigya.com/).
 * It allows you to use the capabilities of Gigya without having to know the internal specifics.
 * Make sure to call Tn.Users.initialize when you page starts up
 *
 * Example of initializing the users class and checking for logged in user :
 *
 *     @example
 *     $("body").append("<p>Initializing user class...</p>");
 *     Tn.Users.initialize({
 *     	onStartup: function(uid) {
 *     		if (!uid) {
 *     			$("body").append("<p>No user logged in</p>");
 *     			return;
 *     		}
 *     		$("body").append(Tn.fm("<p>Logged in user is {0}</p>", uid));
 *     	}
 *     });
 *
 * This example shows how to initiate a login when a link is clicked
 *
 *     @example
 *     $('body').append('<a href"#">Click me to log in</a>').click(function() {
 *     	Tn.Users.logIn();
 *     });
 */
(function(Tn, jQuery, gigya, document, window, undefined) {
	if (!gigya) {
		console.error("Gigya not installed, user library not loaded!");
		return;
	}
	Tn.Users = {
		prefs: {},

		/**
		 * Sets a preference on the system
		 * @param {String} name  The name of the preference
		 * @param {Mixed} value The value of the preference
		 */
		setPref: function(name, value) {
			if ($.isPlainObject(name)) {
				$.extend(this.prefs, name);
			}
			else {
				this.prefs[name] = value;
			}
			this.syncPrefs();
		},

		/**
		 * Returns the preference by the given name
		 * @param  {String} name     The name of the preference
		 * @param  {Mixed} defValue The default value of the preference if not found
		 * @return {Mixed}          The value of the preference
		 */
		getPref: function(name, defValue) {
			if (this.prefs[name] !== undefined) {
				return this.prefs[name];
			}
			return defValue;
		},

		/**
		 * Removes the preference specified
		 * @param  {String} name The name of the preference
		 */
		removePref: function(name) {
			var me = this,
				prefs = [];
			name = $.isArray(name) ? name : [name];
			$.each(name, function(key, val) {
				prefs.push(val);
				delete me.prefs[val];
			});

			// If no user currently logged in, we can't sync
			if (!Tn.currentUser) {
				return;
			}
			gigya.socialize.delUserSettings({
				settings: prefs.join(","),
				group: "preferences"
			});
		},

		/**
		 * Clears all preferences from the queue
		 */
		clearPrefs: function() {
			// If we are currently logged in, sync with the server
			if (Tn.currentUser) {
				var prefs = [];
				$.each(this.prefs, function(key) {
					prefs.push(key);
				});
				gigya.socialize.delUserSettings({
					settings: prefs.join(','),
					group: "preferences"
				});
			}
			this.prefs = {};
		},

		/**
		 * @private
		 * Syncs all preferences with the user service if logged in
		 */
		syncPrefs: function() {
			if (!this.syncTimeout) {
				this.syncTimeout = setTimeout($.proxy(this.syncPrefsImmediately, this), 2000);
			}
			//this.syncPrefsImmediately();
		},

		/**
		 * Synchs the preferences immediately, regardless of timer
		 */
		syncPrefsImmediately: function() {
			// remove the sync timeout 
			if (this.syncTimeout) {
				clearTimeout(this.syncTimeout);
				delete this.syncTimeout;
			}

			// If no user currently logged in, we can't sync
			if (!Tn.currentUser) {
				return;
			}
			gigya.socialize.setUserSettings({
				settings: this.prefs,
				group: 'preferences',
				callback: function(o) {
					if (o.errorCode !== 0) {
						console.error("Error adding preferences", o.errorDetails);
						return;
					}
					//console.error("Saved settings successfully");
					return;
				}
			});
		},

		/**
		 * The default set of reactions to be used across the board
		 * @cfg {Array}
		 */
		defaultReactions: [{
			text: 'Inspiring',
			ID: 'inspiring',
			iconImgUp: 'https://cdns.gigya.com/gs/i/reactions/icons/Inspiring_Icon_Up.png',
			tooltip: 'This is inspiring',
			feedMessage: 'Inspiring!',
			headerText: 'You think this is inspiring'
		}, {
			text: 'Innovative',
			ID: 'innovative',
			iconImgUp: 'https://cdns.gigya.com/gs/i/reactions/icons/Innovative_Icon_Up.png',
			tooltip: 'This is innovative',
			feedMessage: 'This is innovative',
			headerText: 'You think this is Innovative'
		}, {
			text: 'LOL',
			ID: 'lol',
			iconImgUp: 'https://cdns.gigya.com/gs/i/reactions/icons/LOL_Icon_Up.png',
			tooltip: 'LOL',
			feedMessage: 'LOL!',
			headerText: 'You LOL'
		}, {
			text: 'Amazing',
			ID: 'amazing',
			iconImgUp: 'https://cdns.gigya.com/gs/i/reactions/icons/Amazing_Icon_Up.png',
			tooltip: 'This is amazing',
			feedMessage: 'This is amazing!',
			headerText: 'You think this is Amazing'
		}],

		/**
		 * The link to be used for the default reaction
		 * @cfg {String} [reactionLink='http://tbs.com']
		 */
		reactionLink: 'http://tbs.com',

		/**
		 * The title to be used for the default reaction
		 * @cfg {String}
		 */
		reactionTitle: 'TBS Superstation',

		/**
		 * The id of the container to be used to show reactions, or undefined for no reactions
		 * @cfg {String}
		 */
		reactionContainerId: undefined,

		/**
		 * The server defined category, or undefined for no default comment
		 * @cfg {String} [commentCategory=undefined]
		 */
		commentCategory: undefined,

		/**
		 * The container id to display default comments, or undefined for no comments
		 * @cfg {String}
		 */
		commentId: undefined,

		/**
		 * Returns whether a user is logged
		 * @return {Boolean} True if their is a user already logged in
		 */
		isUserLoggedIn: function() {
			return Tn.currentUser ? true : false;
		},

		/**
		 * Sets up a reaction bar at the id specified
		 * @param  {String} id    The id of the div to add the reaction bar to
		 * @param  {String} link  The link for the user action
		 * @param  {String} title The title for the user action
		 */
		setupReactions: function(id, link, title) {
			var act = new gigya.socialize.UserAction();
			act.setLinkBack(link);
			act.setTitle(title);

			gigya.socialize.showReactionsBarUI({
				barID: 'barID',
				showCounts: 'top',
				containerID: id,
				reactions: this.defaultReactions,
				userAction: act,
				showSuccessMessage: true,
				noButtonBorders: false
			});
		},

		/**
		 * @private
		 * Sets up callbacks on the default login methods
		 */
		setupLogin: function() {
			$('.signUp').click(function(event) {
				event.preventDefault();
				gigya.accounts.showScreenSet({
					screenSet: 'Mobile-login',
					mobileScreenSet: 'Mobile-login'
				});
			});
			$('.logIn').click(function(event) {
				event.preventDefault();
				Tn.Users.logIn();
			});
			$('.editProfile').click(function(event) {
				event.preventDefault();
				var screenSet = 'Profile-web';
				if ($(window).width() < 600) {
					screenSet = 'Mobile-profile';
				}
				gigya.accounts.showScreenSet({
					screenSet: screenSet,
					onLogin: function() {

					}
				});
			});
			$('.logOut').click(function(event) {
				event.preventDefault();
				Tn.Users.logOut();
			});
		},

		/**
		 * Logs the user into the system
		 */
		logIn: function() {
			var screenSet = 'Login-web';
			if ($(window).width() < 600) {
				screenSet = 'Mobile-login';
			}
			gigya.accounts.showScreenSet({
				screenSet: screenSet,
				onLogin: function() {}
			});
		},

		/**
		 * Logs any logged in user out of the system
		 */
		logOut: function() {
			console.error("Logging out of gigya");
			this.syncPrefsImmediately();
			gigya.accounts.logout({});
		},

		/**
		 * Adds a comment UI at the id specified
		 * @param  {String} id       The id of the dom element to place comments
		 * @param  {String} category The server generated id for the comment
		 */
		setupComments: function(id, category) {
			gigya.comments.showCommentsUI({
				categoryID: category,
				streamID: '',
				containerID: id,
				deviceType: 'mobile',
				cid: '',
				version: 2,
				useSiteLogin: true
				//,enabledShareProviders: 'facebook,twitter,yahoo,linkedin'
			});
		},

		/**
		 * Initializes the user management system
		 * @param  {Object} config overrides to our default configuration values
		 */
		initialize: function(config) {
			var me = this;
			jQuery.extend(Tn.Users, config || {});

			// Setup reactions if there is a container id specified
			if (me.reactionContainerId) {
				me.setupReactions(me.reactionContainerId, me.reactionLink, me.reactionTitle);
			}

			if (me.commentCategory && me.commentId) {
				me.setupComments(me.commentId, me.commentCategory);
			}

			// Add our callbacks for logging in/out
			me.setupLogin();
		}
	};

	// Run the following commands when the application first starts up
	$(function() {

		// Hide the logout and edit profile UI's until we authenticate for the first time
		$('.logOut, .editProfile, .visible-logged-in').hide();

		// Add event handlers to look for then the user logs in/out of gigya
		gigya.accounts.addEventHandlers({
			onLogin: function(user) {
				Tn.currentUser = user;
				$('.logIn, .signUp, .visible-logged-out').hide();
				$('.logOut, .editProfile, .visible-logged-in').show();
				$('.editProfile').html(Tn.fm("<span class='glyphicon glyphicon-user'>&nbsp;</span>{firstName}", user.profile));

				// Grab the user settings so that they are available as preferences before notifying of login
				gigya.socialize.getUserSettings({
					group: 'preferences',
					callback: function(o) {
						if (o.errorCode !== 0) {
							console.error("Gigya::Error getting user settings", o);
						}
						else {
							Tn.Users.prefs = o.settings;
							console.error("Prefs", Tn.Users.prefs);
						}
						if (Tn.useLoggedInCb) {
							Tn.useLoggedInCb();
						}

						if (Tn.Users.onStartupWatch) {
							clearTimeout(Tn.Users.onStartupWatch);
							Tn.Users.onStartupWatch = null;
							Tn.Users.onStartup(Tn.currentUser.UID);
						}
					}
				});
			},
			onLogout: function() {
				delete Tn.currentUser;
				$('.logIn, .signUp, .visible-logged-out').show();
				$('.logOut, .editProfile, .visible-logged-in').hide();
				if (Tn.useLoggedOutCb) {
					Tn.useLoggedOutCb();
				}
			}
		});

		// When dealing with RaaS, we have to notify social that we're logged in otherwise the plugins won't retain current user
		gigya.accounts.getAccountInfo({
			callback: function(user) {
				if (user.errorCode === 0) {
					gigya.socialize.notifyLogin({
						siteUID: user.UID,
						timestamp: user.signatureTimestamp,
						UIDSig: user.UIDSignature
					});

					if (Tn.Users.onStartup) {
						// Wait 30 seconds for preferences and such to be pulled down before calling the startup method
						Tn.Users.onStartupWatch = setTimeout(function() {
							delete Tn.Users.onStartupWatch;
							console.error("Gigya: Error loading user preferences");
							Tn.Users.onStartup(Tn.currentUser.UID);
						}, 30000);
					}
				}
				else {
					if (Tn.Users.onStartup) {
						Tn.Users.onStartup(null);
					}
					return;
				}
			}
		});

	});

})(Tn, jQuery, window.gigya, document, window);
