(function(Tn, $, document, window, undefined) {

	/**
	 * @class Tn.Video
	 * @singleton
	 * Helper method for handling video
	 */
	Tn.Video = {
		width: 614,
		height: 345,
		maxHeight: 400,
		widthOffsetRatio: 1.0,
		autoStart: true,
		contentId: 'adultswim1',

		/**
		 * Initializes the video player with the specified content id
		 * @private
		 * @param  {String} contentId The content id
		 */
		setContent: function(contentId) {
			var me = this,
				json, vid;
			if (Tn.debugMode) {
				json = '{"id":"adultswim1","timestamp":"Thu Feb 12 18:44:34 EDT 2009","category":"world","slug":"adultswim1","headline":"Family Guy: Game Night","description":"This is a <b>test description</b> for adultswim video. Family Guy is fun. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","files":[{"file":{"bitrate":"300","type":"standard","text":"http://httpstream.turner.com/contents/cvp/adultswim1.flv"}},{"file":{"bitrate":"600","type":"hd","text":"http://httpstream.turner.com/contents/cvp/adultswim1.flv"}}],"images":[{"image":{"height":"240","width":"320","name":"bb","text":"http://collabspace.turner.com/dmtvideo/test_content/thumbnails/AdultSwim1_576x324.jpg"}},{"image":{"height":"324","width":"576","name":"lg","text":"http://collabspace.turner.com/dmtvideo/test_content/thumbnails/AdultSwim1_576x324.jpg"}},{"image":{"height":"360","width":"640","name":"lg","text":"http://collabspace.turner.com/dmtvideo/test_content/thumbnails/AdultSwim1_576x324.jpg"}},{"image":{"height":"270","width":"480","name":"xlg","text":"http://collabspace.turner.com/dmtvideo/test_content/thumbnails/AdultSwim1_576x324.jpg"}}],"trt":"439","toggleFullscreenQuality":"off","clickbackUrl":"http://www.sikids.com","relatedVideosUrl":"http://collabspace.turner.com/dmtvideo/test_content/xml/feed.xml","metas":{"site":"nascar","version":"20091113","category":"preview","subCategory":"sound_off","fullBranding":"preview_sound_off","categoryFullname":"Preview","subCategoryFullname":"Sound Off","adTitle":"NASCAR Videos / Cup / Preview / Sound Off","branding":"","keywords":""}}';
			}
			else {
				json = Tn.player.getContentEntry(contentId);
			}
			vid = Tn.decode(json);
			Tn.currentVideo = vid;
			$('.vid-title').html(vid.headline);
			$('.vid-description').html(vid.description);

			$('.loading').hide();
			$('.loading2').fadeOut(1500);
			// $('#'+me.videoFrame).animate({
			//	opacity: "1.0"
			// }, 1000);

			$.ajax({
				//url: vid.relatedVideosUrl,
				url: 'feed.xml',
				success: function(xml) {
					var vids = Tn.xml2json(xml);
					if (!vids || !vids.videos || !vids.videos.video) {
						return;
					}
					var recent = me.buildRecent(vids.videos.video);
					$('.vid-recent').html(recent);
				},
				fail: function() {
					console.error("FAILED TO LOAD RELATED CONTENT");
				}
			});
		},

		buildRecent: function(vids) {
			var output = '<ul class="post-grid-3">';
			$.each(vids, function(index, vid) {
				output += '<li class="item cf item-video">';
				output += Tn.fm(
					'<div class="thumb"><a class="clip-link" title="{0}" href="{4}"><span class="clip"><img src="{1}" alt="{1}"><span class="vertical-align"></span></span><span class="overlay"></span></a></div><div class="data"><h4 class="entry-title"><a class="clip-link" href="{4}" title="{0}">{0}</a></h4><p class="meta"><span class="time">{3}</span></p></div>',
					vid.headline.value,
					vid.thumbnail_url.value,
					vid.description.value,
					vid.duration.value,
					vid.id.value
				);
				output += '</li>';
			});
			output += '</ul>';
			return output;
		},

		/**
		 * Resizes the video player to the current coontainer dimensions
		 */
		onResize: function() {
			var me = this;
			me.onResizeInternal();
			/*			// There was some freezing noticed with CVP when doing rapid resizing,
			// so I changed it so that resizing only occurs when the view has settled down.
			if (me.resizeTimer) {
				clearTimeout(me.resizeTimer);
			}
			me.resizeTimer = setTimeout(jQuery.proxy(me.onResizeInternal, me), 500);*/
		},

		/**
		 * Monitors the video player for a resize for the amount of milli seconds specified
		 * @param  {Number} lengthInMs The amount of millisecons to keep  monitoring the video
		 */
		monitorResize: function(lengthInMs) {
			this.monitorResizeEnd = new Date().getTime() + lengthInMs;
			this.monitorResizeInternal();
		},

		/**
		 * @private
		 */
		monitorResizeInternal: function() {
			if (this.monitorResizeTimer) {
				clearTimeout(this.monitorResizeTimer);
			}
			this.onResizeInternal();
			if (!this.monitorResizeEnd) {
				return;
			}
			if (new Date().getTime()>this.monitorResizeEnd) {
				delete this.monitorResizeEnd;
				return;
			}
			this.monitorResizeTimer = setTimeout($.proxy(this.monitorResizeInternal, this), 10);
		},

		onResizeInternal: function() {
			var me = this;
			me.resizeTimer = null;

			if (!me.playerReady || !Tn.player) {
				return;
			}

			var $video = $('#' + me.videoId);
			var $content = $('.content');
			var $container = $('#' + me.playerDivId);

			// jQuery returning wrong normalized width and height values, so go with the source
			//var w = window.innerWidth || document.body.clientWidth || document.documentElement.clientWidth;
			var w = $container.width();
			var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

			var minHeight = h * 1.0;
			var maxHeight = Tn.Video.maxHeightCb ? Tn.Video.maxHeightCb() : Tn.Video.maxHeight;

			var vw = w * me.widthOffsetRatio;
			var vh = vw * 360 / 640;

			if (vh > minHeight * 1.0) {
				vh = minHeight;
				vw = vh * 640 / 360;
			}
			if (vh > maxHeight) {
				vh = maxHeight;
				vw = vh * 640 / 360;
			}

			vw = parseInt(vw, 10);
			vh = parseInt(vh, 10);

			if (vw < 100 || vh < 100) {
				//console.error("Found bad size");
				return;
			}

			if (me.matchPlayerHeight || me.fit) {
				vw = $container.width();
				vh = $container.height();
			}

			Tn.player.resize(vw, vh, 0);

			$video.width(vw).height(vh);
			//$title.width(vw);
			$content.width(vw);
			//$title.show();


			$('#' + me.videoFrame).height(vh).width(vw);
			$('.matchheight').height(vh);
			$('.matchwidth').width(vw);
			$('div.wrap.cf').width(vw);
		},

		onPlayTrackingPrivate: function() {
			var me = this;
			if (me.onPlayTracking) {
				me.onPlayTracking();
			}
			me.onPlayTrackingTimer = setTimeout($.proxy(me.onPlayTrackingPrivate, me), 1000);
		},

		/**
		 * CVP doesn't have an onContentResume callback, so we have to call this manually
		 * @return {[type]} [description]
		 */
		onContentResume: function() {
			var me = this;
			if (!me.onPlayTracking) {
				return;
			}
			if (me.onPlayTrackingTimer) {
				clearTimeout(me.onPlayTrackingTimer);
				me.onPlayTrackingTimer = null;
			}
			me.onPlayTrackingPrivate();
		},

		/**
		 * Create a video player with the specified config options
		 * @param  {Object} config [description]
		 */
		createPlayer: function(config) {
			var container, embedId, me = this;

			jQuery.extend(me, config);

			// Make sure the user specified a div to store the video under
			if (!me.playerDivId) {
				console.error("No player div id specified");
				return;
			}

			// Ensure the container specified actually exists
			container = $('#' + me.playerDivId);
			if (container.length === 0) {
				return;
			}

			//me.videoId = me.playerDivId + '-video';
			me.videoId = 'cvp_1';
			me.videoFrame = me.videoId + '-frame';
			embedId = me.videoId + '-temp';
			container.prepend(Tn.fm('<div id="{1}"" style="opacity: 1; -moz-box-shadow: 0px 0px 8px 2px #000; -webkit-box-shadow: 0px 0px 8px 2px #000; box-shadow: 0px 0px 8px 2px #000; position: relative; width: 100%; border: 0px solid red;"><div class="container" id="{0}"></div>', embedId, me.videoFrame));

			// Allow users to embed their anntations on top the video
			if (me.embedContentCb) {
				me.embedContentCb($('#' + me.videoFrame));
			}

			Tn.player = new CVP({
				id: me.videoId,
				width: me.width,
				height: me.height,
				freewheelJsUrl: 'http://z.cdn.turner.com/xslo/cvp/ads/freewheel/js/AdManager_5.0.3.js',
				flashVars: {
					context: 'main',
					autostart: me.autoStart ? 'true' : 'false',
					site: 'cvp',
					profile: '1',
					contentId: Tn.debugMode ? undefined : me.contentId,
					wmode: 'transparent'
				},

				embed: {
					containerSwf: 'cvp_eval_container.swf',
					expressInstallSwf: 'http://i.cdn.turner.com/xslo/cvp/assets/flash/expressInstall.swf',
					options: {
						quality: 'high',
						bgcolor: '#000',
						allowFullScreen: 'true',
						allowScriptAccess: 'always',
						wmode: 'transparent'
					}
				},

				onPlayerReady: function() {
					Tn.Video.playerReady = true;
					//console.error("Video player ready", arguments);
					me.onResize();
					$(window).resize($.proxy(Tn.Video.onResize, Tn.Video));
					Tn.player.setAutoBitrateSwitch(true);
				},

				onCVPReady: function() {
					//console.error("CVP player ready", arguments);
					//player.play('adultswim1');
				},

				onContentBegin: function(playerId, contentId) {
					me.setContent(contentId);
				},

				onContentPlay: function() {
					if (me.onContentPlay) {
						me.onContentPlay();
					}

					me.onContentResume();
				},

				onContentEnd: function() {
					if (me.onContentEnd) {
						me.onContentEnd();
					}
				},

				onContentPause: function() {
					if (me.onContentPause) {
						me.onContentPause();
					}

					if (me.onPlayTrackingTimer) {
						clearTimeout(me.onPlayTrackingTimer);
						me.onPlayTrackingTimer = null;
					}
				},

				// Ads
				onAdPlay: function() {
					//console.error(this.caller);
				},
				onAdEnd: function() {
					//console.error(this.caller);
				},
				onAdError: function() {
					//console.error(this.caller);
				},

				// Tracking methods
				onTrackingContentProgress: function() {
					//console.error("Tracking ad", arguments);
				},
				onTrackingPaused: function() {
					//console.error("Tracking paused", arguments);
				}
			});

			Tn.player.embed(embedId);
			if (Tn.debugMode) {
				me.setContent('adultswim1');
			}
		}
	};

	// Capture any video links tha we clicked on
	$('body').on('click', '.clip-link', function(event) {
		event.preventDefault();

		if (!Tn.player) {
			return;
		}

		Tn.player.play($(this).attr('href'));
		$("html, body").animate({
			scrollTop: 0
		}, "slow");
	});

})(Tn, jQuery, document, window);
