(function(window, $, Tn, undefined) {

	/**
	 * @class  Tn.button.Logo
	 * A button which is uses as an icon only with minimal dom elements
	 */
	Tn.define('Tn.button.Logo', {
		alias: 'widget.logobutton',
		extend: 'Tn.button.Button',
		baseElement: 'a',
		baseCls: 'navbar-brand',
		
		constructor: function() {
			this.superClass.apply(this, arguments);
		}
	});

})(window, jQuery, Tn);