(function(window, $, Tn, undefined) {

    /**
     * @class Tn.button.Button
     * The base class for all buttons
     */
    Tn.define('Tn.button.Button', {
        alias: 'widget.button',
        extend: 'Tn.Component',

        /**
         * If specified, only one button from this set will remain active
         * @cfg {Boolean} toggleGroup
         */
        toggleGroup: undefined,
        keepState: false,
        pressed: false,
        pressedCls: 'active',

        /**
         * Sets the pressed state for this button
         * @param {Boolean} val If true, then the button will appear pressed
         */
        setPressed: function(val) {
            this.pressed = val;
            this.$el.toggleClass(this.pressedCls, this.pressed);
        },

        /**
         * @protected
         * Internal handler for when the user clicks a button
         */
        onClickHandler: function() {
            var me = this;
            if (me.keepState) {
                me.pressed = !me.pressed;

                if (me.toggleGroup) {
                    $('.' + me.toggleGroup).each(function() {
                        var cmp = Tn.getCmp(this.id);
                        if (!cmp) {
                            return;
                        }
                        if (cmp === me) {
                            return;
                        }
                        if (cmp.setPressed) {
                            cmp.setPressed(false);
                        }
                    });
                }

                me.setPressed(true);
            }
            if (me.handler) {
                me.handler.call(me);
            }
            me.fireEvent('click');
        },

        /**
         * Sets the text value for this button
         * @param {String} val The new value for this button
         */
        setText: function(val) {
            this.text = val;
            this.updateLayout();
        },

        updateLayout: function() {
            var text = this.text || '';
            if (this.icon) {
                text = Tn.fm("<img src='{0}' style='width:25px;height:25px;margin:0;'>", this.icon) + text;
            }
            this.$el.html(text);
        },
        
        constructor: function() {
            this.superClass.apply(this, arguments);

            this.$el.on("click", $.proxy(this.onClickHandler, this));

            this.updateLayout();

            if (this.toggleGroup) {
                this.addCls(this.toggleGroup);
            }
            if (this.pressed) {
                this.addCls(this.pressedCls);
            }
        }
    });

})(window, jQuery, Tn);
