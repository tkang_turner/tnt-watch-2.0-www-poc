(function(window, $, Tn, undefined) {

	/**
	 * @class Tn.button.NavButton
	 * A navigation button which uses a list item as the base dom element
	 */
	Tn.define('Tn.button.NavButton', {
		alias: 'widget.navbutton',
		extend: 'Tn.button.Button',
		baseElement: 'li',
		
		updateLayout: function() {
            var text = Tn.fm("<a href='#'>{0}</a>", this.text || '');
            if (this.icon) {
                text = Tn.fm("<img src='{0}' style='width:25px;height:25px;margin:0;'>", this.icon) + text;
            }
            this.$el.html(text);
		},

		constructor: function(config) {
			config = config || {};
			this.superClass.apply(this, arguments);
		}
	});

})(window, jQuery, Tn);