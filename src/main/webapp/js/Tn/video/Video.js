(function(window, $, Tn, undefined) {

	/**
	 * @class  Tn.widgets.Video
	 * An image that represents a movie or page a user can click on
	 */
	Tn.define('Tn.widgets.Video', {
		extend: 'Tn.Container',
		alias: ['widget.video','widget.movie'],

		/**
		 * The current mode of this button, can be "play", "soon", "zombie"
		 * @cfg {String} mode
		 */
		mode: undefined,

		/**
		 * The source image for this thumbnail
		 * @cfg {String} image
		 */
		image: undefined,

		maxDescLength: 200,
		maxDescText: '...',
		expireText: 'EXPIRES',
		imgTpl: '<img src="{thumb}">',
		lazyImgTpl: '<img class="lazyOwl" data-src="{thumb}">',
		lazyLoad: false,

		movieTpl: [
			'<div class="modemsg">{mode}</div>',
			'<div class="playbut"></div>',
			'<p class="title">{maintitle}</p>',
			'<ul class="buttons">',
			// '	<li><a class="play" href="#"><span class="glyphicon glyphicon-play black"></a></li>',
			'	<li><a class="watch" href="#">+</a></li>',
			'	<li><a class="fullinfo" href="#">i</a></li>',
			'</ul>',
			'<div class="moreinfo">',
			'	<div class="desc">',
			'		<h3>{title}</h3>',
			'		<p>{rating}  {duration}</p>',
			'		<p>{desc}</p>',
			'		<div class="links">',
			'			<p>{expiration}</p>',
			'			<ul class="smallbuttons">',
			'				<li><a class="play" href="#"><span class="glyphicon glyphicon-play white"></a></li>',
			'				<li><a class="watch" href="#">+</a></li>',
			'				<li><a class="fullinfo" href="#">i</a></li>',
			'			</ul>',
			'		</div>',
			'	</div>',
			'</div>'
		],

		movieTplMinimal: [
			'<p class="title">{maintitle}</p>',
			'<div class="playbut"></div>',
			'<ul class="buttons">',
			'	<li><a class="watch" href="#">+</a></li>',
			'	<li><a class="info" href="#">i</a></li>',
			'</ul>',
			'<div class="moreinfo">',
			'	<div class="desc">',
			'		<p>{desc}</p>',
			'		<div class="links">',
			'			<p>{title}</p>',
			'			<ul class="smallbuttons">',
			'				<li><p>{rating}  {duration}</p></li>',
			'			</ul>',
			'		</div>',
			'	</div>',
			'	<div class="infoarea">',
			'		<a href="#" class="play"><span style="font-size:64px;text-shadow: black 0em 0em 0.1em;" class="glyphicon glyphicon-play-circle white"></a><br>',
			'		<a href="#" class="fullinfo"><span style="font-size:24px;text-shadow: black 0em 0em 0.1em;" class="glyphicon glyphicon-info-sign white"></a>',
			'		<a href="#" class="watch"><span style="margin-left:16px; font-size:24px;text-shadow: black 0em 0em 0.1em;" class="glyphicon glyphicon-plus-sign white"></a>',
			'	</div>',
			'</div>'
		],

		movieTplMinimalPlay: [
			'<p class="title">{maintitle}</p>',
			'<div class="playbut"></div>',
			'<ul class="buttons">',
			'	<li><a class="watch" href="#">+</a></li>',
			'	<li><a class="info" href="#">i</a></li>',
			'</ul>',
			'<div class="moreinfo">',
			'	<div class="desc">',
			'		<div class="links">',
			'			<p>{title}</p>',
			'			<ul class="smallbuttons">',
			'				<li><p>{rating}  {duration}</p></li>',
			'			</ul>',
			'		</div>',
			'	</div>',
			'	<div class="infoarea">',
			'		<a href="#" class="play"><span style="font-size:64px;text-shadow: black 0em 0em 0.1em;" class="glyphicon glyphicon-play-circle white"></a><br>',
			'		<a href="#" class="fullinfo"><span style="font-size:24px;text-shadow: black 0em 0em 0.1em;" class="glyphicon glyphicon-info-sign white"></a>',
			'		<a href="#" class="watch"><span style="margin-left:16px; font-size:24px;text-shadow: black 0em 0em 0.1em;" class="glyphicon glyphicon-plus-sign white"></a>',
			'	</div>',
			'</div>'
		],

		showInfo: function() {
			this.$el.addClass("active");
			return false;
		},
		onClick: function() {
			this.fireEvent("click", this);
			return false;
		},
		addWatch: function() {
			this.fireEvent("watch", this);
			return false;
		},
		showFullInfo: function() {
			this.fireEvent("info", this);
			return false;
		},
		constructor: function(config) {
			config.cls = (config.cls || '') + ' item';
			this.superClass.apply(this, arguments);
			if (!this.maintitle) {
				this.maintitle = this.title;
			}
			if (this.desc.length>this.maxDescLength) {
				this.desc = this.desc.substring(0, this.maxDescLength) + this.maxDescText;
			}
			if (this.expiration) {
				this.expiration = this.expireText + ' ' + this.expiration;
			}
			var tpl;
			tpl = Tn.applyTpl(this.lazyLoad ? this.lazyImgTpl : this.imgTpl, this);
			tpl.appendTo(this.$el);
			if (!this.minimal) {
				tpl = Tn.applyTpl(this.movieTpl, this);
			} else {
				if (this.minimal==='playonly') {
					tpl = Tn.applyTpl(this.movieTplMinimalPlay, this);
				} else {
					 tpl = Tn.applyTpl(this.movieTplMinimal, this);
				}
			}
			tpl.appendTo(this.$el);
			this.$el.find('.play').click($.proxy(this.onClick, this));
			this.$el.find('.info').click($.proxy(this.showInfo, this));
			this.$el.find('.fullinfo').click($.proxy(this.showFullInfo, this));
			this.$el.find('.watch').click($.proxy(this.addWatch, this));
			this.$el.click($.proxy(this.onClick, this));
		}
	});

})(window, jQuery, Tn);
