(function(window, $, Tn, undefined) {

	/**
	 * @class  Tn.continuum.Header
	 * Creates a header based on the Continuum header
	 */
	Tn.define('Tn.continuum.Header', {
		alias: 'widget.ctheader',
		extend: 'Tn.bootstrap.Header',

		constructor: function(config) {
			config = config || {};
			this.superClass.apply(this, arguments);
			this.addButton([
				{
					cls: 'hidden-xs',
					text: '<span class="glyphicon glyphicon-search"></span>'
				},
				{
					cls: 'visible-xs',
					text: 'search'
				}
			]);
		}
	});

})(window, jQuery, Tn);
