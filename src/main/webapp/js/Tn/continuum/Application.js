(function(window, $, Tn, undefined) {

	function buildApp() {
		// Check to see if we're in edit mode
		if (Tn.gup("method")!=="") {
			Tn.buildXtypes();
		} else {
			Tn.buildXtypes("<div class='tn-hidden'></div>");
		}
		var pages = $('.tn-page');
		if (pages.length>0) {
			var page = Tn.getCmp(pages.attr('id'));
			if (page) {
				page.show();
			}
		}
	}

	// TeamSite preview blocks ready and load events, so do it like this for now
	function checkAppState() {
		if (document.readyState!=='complete') {
			setTimeout(checkAppState, 100);
			return;
		}
		buildApp();
	}

	checkAppState();

})(window, jQuery, Tn);
