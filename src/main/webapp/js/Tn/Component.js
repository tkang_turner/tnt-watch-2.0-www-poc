(function(window, $, Tn, undefined) {

	/**
	 * @class Tn.Observable
	 * Class used for handling events in the system
	 */
	Tn.define("Tn.Observable", {
		internalListeners: [],

		/**
		 * Fires an event on this object
		 * You can specify any number of arguments after the name
		 * @param  {String} name The name of the event to fire
		 */
		fireEvent: function(name) {
			var me = this, args = [];
			for (var i=1; i<arguments.length; i++) {
				args.push(arguments[i]);
			}
			$.each(this.internalListeners, function(key, evt) {
				if (evt.name!==name) {
					return;
				}
				evt.fn.apply(evt.scope || me, args);
			});
		},

		/**
		 * @private
		 * Private method fo radding a listener
		 */
		addListenerInternal: function(name, fn, scope, options) {
			this.internalListeners.push({
				name: name,
				fn: fn,
				scope: scope,
				options: options
			});
		},

		/**
		 * Adds a listern to th queue
		 * You can add multiple listeners by specifying an object with name and fn. In the object, 
		 * you can still add "scope" to make sure the correct scrope is applied per function.
		 * 
		 * @param {String}   name    The name of the listener to add
		 * @param {Function} fn      The callback function to be called, if none specified, uses the current object
		 * @param {Object}   [scope]   The scope which to run this method 
		 * @param {Object}   [options] Extra options to specify
		 */
		addListener: function(name, fn, scope, options) {
			if (typeof name==='string') {
				this.addListenerInternal(name, fn, scope, options);
				return;
			}
			$.each(name, function(key, val) {
				if (typeof key==='string') {
					return;
				}
				this.addListenerInternal(key, val, name.scope, {
					single: (typeof name.single===undefined) ?  false : name.single
				});
			});
		},

		/**
		 * Remove the listener from this object
		 * @param  {String}   name The name of the event to remove
		 * @param  {Function} [fn] A function reference. If none specified, all events by that method name will be removed 
		 */
		removeListener: function(name, fn) {
			var arr = [];
			$.each(this.internalListeners, function(key,val) {
				if ((val.name!==name && !fn) || (val.name===name && fn && fn!==val.fn)) {
					arr.push(val);
					return;
				}
			});
			this.internalListeners = arr;
		},

		/**
		 * Removes all references to listeners
		 */
		clearListeners: function() {
			this.internalListeners = [];
		}
	});

	/**
	 * @class Tn.Component
	 * Base class for all Turner components
	 *
	 * The example below will show how to instantiate a component, however the typical use case is to sub class a component.
	 *
	 *      @example
	 *      new Tn.Component({
	 *     	 renderTo: document.body,
	 *     	 html: 'This is how a basic component can be used to display html'
	 *      });
	 */
	Tn.define('Tn.Component', {
		alias: ['widget.component'],
		mixins: {
			queryable: "Tn.Observable"
		},

		/**
		 * @private
		 * Let's the system know that we are a component
		 * @type {Boolean}
		 */
		isComponent: true,

		/**
		 * If specified, the component will be rendered to the specified container
		 * Can be an id, or another component
		 * @cfg {String/Tn.Component/HTMLElement} renderTo
		 */
		renderTo: undefined,

		/**
		 * If specified, the component is initialized with the following html
		 * @cfg {String} html
		 */
		html: undefined,

		/**
		 * @protected
		 * The base element this component is composed of
		 * @cfg {String} [baseElement=div]
		 */
		baseElement: 'div',

		/**
		 * An optional id used to specify a DOM id for the component
		 * @cfg {String} id
		 */
		id: undefined,

		/**
		 * @protected
		 * The jQuery element for this component
		 * @type {String/HTMLElement}
		 */
		$el: undefined,

		/**
		 * @private
		 * The parent of this component if it's attached to another component
		 * @type {Tn.Component}
		 */
		parent: undefined,

		/**
		 * @protected
		 * The base cls used for this comonent, defaults to the base xtype
		 * @cfg {String} baseCls
		 */
		baseCls: undefined,

		/**
		 * An optional user defined class that gets added to this component
		 * @cfg {String} cls
		 */
		cls: undefined,

		/**
		 * A string or object of styles to be applied to this component
		 * @type {String/Object}
		 */
		style: undefined,

		/**
		 * If true, then this component is initialized as hidden
		 * @cfg {Boolean} hidden
		 */
		/**
		 * Tells the current state of this component
		 * @type {Boolean}
		 */
		hidden: undefined,

		/**
		 * Optionally specifies the width of this component
		 * Pixels will be used if a number is passed in. To use any other unit, specify it as a string with the html unit.
		 * @cfg {Number} width
		 */
		width: undefined,

		/**
		 * Optionally specifies the height of this component
		 * Pixels will be used if a number is passed in. To use any other unit, specify it as a string with the html unit.
		 * @cfg {Number} height
		 */
		height: undefined,

		/**
		 * Specifies the default xtype for every newly created component
		 * @cfg {String} [defaultXType=component]
		 */
		defaultXType: 'component',

		constructor: function(config) {
			var me = this;
			config = config || {};

			// Set the proper xtype based on the alias of this component
			if (this.alias) {
				if ($.isArray(this.alias)) {
					this.xtype = this.alias[0].replace('widget.', "");
				}
				else {
					this.xtype = this.alias.replace('widget.', "");
				}
			}
			else {
				console.error("You have an incorrect alias specified");
				this.xtype = 'component';
			}

			// Copy base config options
			$.extend(this, config);

			// If no id specified, generate a unique ID
			if (!this.id) {
				this.id = Tn.uid(this.xtype);
			}
			Tn.ComponentManager.addComponent(this);

			// Initialize the style 
			if (!this.style) {
				this.style = '';
			}
			else if (Tn.isObject(this.style)) {
				var tmpStyle = '';
				$.each(this.style, function(key, val) {
					tmpStyle = key + ":" + val + ";";
				});
				this.style = tmpStyle;
			}

			// Make sure the style ends with a semi colon
			if (this.style.length > 0 && this.style.charAt(this.style.length - 1) !== ';') {
				this.style += ';';
			}

			var uiCls = this.getDefaultClass();

			// If the component is hidden, then update the style
			if (this.hidden === true || this.hidden==='true') {
				uiCls += ' ' + Tn.uiPrefix + 'hidden';
			}
			else {
				this.hidden = false;
			}

			if (this.cls) {
				uiCls += ' ' + this.cls;
			}

			this.$el = $(Tn.fm('<{3} style="{2}" id="{0}" class="{1}">{4}</{3}>',
				this.id, uiCls, this.style || "", this.baseElement, this.html || ""));

			if (this.width) {
				this.$el.width(this.width);
			}
			if (this.height) {
				this.$el.height(this.height);
			}

			if (this.renderTo) {
				this.attachTo(this.renderTo);
			}

			if (this.listeners) {
				$.each(this.listeners, function(key, evt) {
					me.addListener(key, evt, me.listeners.scope, me.listeners.single);
				});
			}

			this.updateLayout();
		},

		/**
		 * Adds the given class to the component
		 * @param {String} cls The class to be added
		 */
		addCls: function(cls) {
			this.$el.addClass(cls);
		},

		/**
		 * Remove the class from the component
		 * @param  {String} cls The class to be removed
		 */
		removeCls: function(cls) {
			this.$el.removeClass(cls);
			return this;
		},

		/**
		 * Shows this component
		 */
		show: function() {
			this.$el.removeClass(Tn.uiPrefix + 'hidden');
			this.hidden = false;
			return this;
		},

		/**
		 * Hides this component
		 */
		hide: function() {
			this.$el.addClass(Tn.uiPrefix + 'hidden');
			this.hidden = true;
			return this;
		},

		/**
		 * Returns whether or not this component is shown
		 * @return {Boolean} True if the component isn't hidden
		 */
		isShown: function() {
			return !this.hidden;
		},

		/**
		 * Toggles the visibility of the component, or set's the visibility to the input value
		 * @param  {Boolean} [show] If true, shows the component, false hides the component
		 * @return {Boolean}      Returns true if the widget was shown
		 */
		toggle: function(show) {
			if (show === undefined) {
				show = !this.hidden;
			}
			if (show) {
				this.show();
			}
			else {
				this.hide();
			}
			return !this.hidden;
		},

		/**
		 * Returns the current width of the component in pixels
		 * @return {Number} Width in pixels
		 */
		getWidth: function() {
			return this.$el.width();
		},

		/**
		 * Returns the current height of the component in pixels
		 * @return {Number} Height in pixels
		 */
		getHeight: function() {
			return this.$el.height();
		},

		/**
		 * Sets the width of the component.
		 * If no unit is specified, it's assumed to be in pixels
		 * See [jQuery documentation](http://api.jquery.com/width/) for more detail
		 */
		setWidth: function(width) {
			this.$el.width(width);
			return this;
		},

		/**
		 * Sets the height of the component.
		 * If no unit is specified, it's assumed to be in pixels
		 * See [jQuery documentation](http://api.jquery.com/height/) for more detail
		 */
		setHeight: function(height) {
			this.$el.height(height);
			return this;
		},

		/**
		 * @protected
		 *
		 * Gets the default class to apply to this component
		 * @return {String} [description]
		 */
		getDefaultClass: function() {
			return this.baseCls || (Tn.uiPrefix + this.xtype);
		},

		/**
		 * Attaches the compoment to the specified
		 * @param  {Mixed} comp The component, dom id, or dom element to attach this component to
		 * @param  {Boolean} [front=false] If specified, attaches to the front
		 */
		attachTo: function(comp, front) {
			switch (typeof comp) {
				case "string":
					var $comp;
					if (comp==='document.body') {
						$comp = $('body');
					} else {
						$comp = $('#' + comp);
					}
					if (front) {
						this.$el.prependTo($comp);
					}
					else {
						this.$el.appendTo($comp);
					}
					break;
				case "object":
					if (comp.isComponent) {
						if (front) {
							this.$el.prependTo(comp.$el);
						}
						else {
							this.$el.appendTo(comp.$el);
						}

						this.parent = comp;
					}
					else {
						if (front) {
							this.$el.prependTo(comp);
						}
						else {
							this.$el.appendTo(comp);
						}
					}
					break;
				default:
					console.error("Can't attach to dom");
			}
			return this;
		},

		/**
		 * Returns the DOM container for this component
		 * @return {Object} DOM of container
		 */
		getDom: function() {
			return this.$el.get(0);
		},

		/**
		 * Destroys this component and cleans up resources
		 */
		destroy: function() {
			Tn.ComponentManager.removeComponent(this);
			this.clearListeners();
			this.$el.remove();
			this.$el = undefined;
			this.html = undefined;
			this.parent = undefined;
		},

		/**
		 * Tells the component to update it's layout with the current DOM
		 */
		updateLayout: function() {},

		/**
		 * Creates the widget for the specified item
		 * @return {Tn.Component} The newly created widget
		 */
		createWidget: function(item) {
			return Tn.create(item, this.defaultXType);
		}
	});

})(window, jQuery, Tn);
