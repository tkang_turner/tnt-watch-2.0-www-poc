(function(window, $, Tn, undefined) {

	/**
	 * @class  Tn.bootstrap.Header
	 * Creates a responsive header based on bootstrap's collapsible navbar
	 */
	Tn.define('Tn.bootstrap.Header', {
		alias: 'widget.header',
		extend: 'Tn.Component',
		baseCls: 'tn-bootstrap-header navbar navbar-inverse navbar-static-top',

		/**
		 * The current active item
		 * @type {Number}
		 */
		activeItem: 0,

		setActiveItem: function(page) {
			var me = this,
				curItem = null;
			switch (typeof page) {
				case 'number':
					page = parseInt(page, 10);
					if (page < 0 || page >= me.items.length) {
						return;
					}
					if (me.activeItem !== page && me.activeItem > 0 && me.activeItem < me.items.length) {
						me.items[me.activeItem].hide();
					}
					me.items[page].show();
					me.activeItem = page;
					break;
				case 'string':
					curItem = Tn.getCmp(page);
					break;
				default:
					curItem = page;
			}
			if (!curItem) {
				return;
			}
			$.each(me.items, function(index, item) {
				if (item === curItem) {
					item.show();
					me.activeItem = index;
					return;
				}
				// if (me.activeItem !== page && me.activeItem > 0 && me.activeItem < me.items.length) {
				// 	me.items[me.activeItem].hide();
				// }
				item.hide();
			});
		},

		/**
		 * Adds a navigation button to the header
		 * @param {Array/Object} item [description]
		 */
		addButton: function(item) {
			var me = this;
			if (!item) {
				return;
			}
			// Check to see if we have an array of items
			if ($.isArray(item)) {
				$.each(item, function() {
					me.addButton(this);
				});
				return;
			}

			item.renderTo = this.$el.find('.nav.navbar-nav.navbar-right');
			item.xtype = 'navbutton';
			item = me.createWidget(item);
		},

		constructor: function(config) {
			config = config || {};

			// Hide each item by default
			config.items = config.items || [];
			$.each(config.items, function(index, item) {
				item.hidden = true;
			});

			// See if this div is being initialied from a content el
			if (config.contentEl) {
				$(config.contentEl).children().each(function() {
					var item = $(this).getAttributes();
					item.text = $(this).text();
					config.items.push(item);
				});
			}

			this.superClass.apply(this, arguments);

			// Add the navbar area
			this.$el.prepend([
				'<div class="container">',
				'  <div class="navbar-header">',
				'    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">',
				'      <span class="sr-only">Toggle navigation</span>',
				'      <span class="icon-bar"></span>',
				'      <span class="icon-bar"></span>',
				'      <span class="icon-bar"></span>',
				'    </button>',
				'  </div>',
				'  <div class="navbar-collapse collapse no-transition">',
				'    <ul class="nav navbar-nav navbar-right">',
				'    </ul>',
				'  </div>',
				'</div>'
			].join(""));

			var me = this,
				navHeader = this.$el.find('.navbar-header'),
				collapseHeader = this.$el.find('.navbar-right');

			$.each(this.items, function(index, item) {
				if (me.navDefaults) {
					Tn.applyIf(item, me.navDefaults);
				}
				item.renderTo = collapseHeader;
				item.xtype = 'navbutton';
				item = me.createWidget(item);
			});

			// If a logo button was specified, then add the logo to the header
			if (this.logo) {
				Tn.create({
					xtype: 'logobutton',
					icon: this.logo,
					pageUrl: this.pageUrl,
					renderTo: navHeader
				});
			}
		}
	});

})(window, jQuery, Tn);
