(function(window, $, Tn, undefined) {

	/**
	 * @class Tn.Dialog
	 * Base type for showing popup dialogs
	 */
	Tn.define('Tn.Dialog', {
		extend: 'Tn.Container',
		alias: 'widget.dialog',

		/**
		 * The template for the dialog to be displayed
		 * @cfg {String}
		 */
		template: [
			//'<div id="{id}-dialog" tabindex="-1" role="dialog" aria-labelledby="{id}-label" aria-hidden="true">',
			'  <div class="modal-dialog">',
			'    <div class="modal-content">',
			'      <div class="modal-header">',
			'        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">',
			'          <span class="glyphicon glyphicon-remove-sign white"></span>',
			'        </button>',
			'        <h4 class="modal-title" id="{id}-label">{title}</h4>',
			'      </div>',
			'      <div id="{id}-body" class="modal-body">',
			'      </div>',
			'    </div>',
			'  </div>'
			//'</div>'
		].join(''),


		getDefaultClass: function() {
			return this.superClass.apply(this, arguments) + ' modal fade';
		},

		constructor: function(config) {
			var me = this;
			config.id = config.id || Tn.uid();
			config.renderTo = null;

			this.superClass.apply(this, arguments);
			this.$el.attr({
				tabindex: -1,
				role: 'dialog',
				'aria-hidden': true,
				'aria-labelledby': this.id + '-label'
			});

			var tplStr = Tn.fm(this.template, {
				id: this.id,
				title: this.title || ''
			});
			$(tplStr).appendTo(this.$el);

			// Make sure the items get rendered in the modal body
			$.each(this.items || [], function(key, item) {
				item.attachTo(me.$el.find('#' + config.id + '-body'));
			});

			this.$el.appendTo($(document.body), true);

			//$('#' + this.id + '-dialog').modal();
			$('#' + this.id).modal({
				show: false
			});
		},

		updateLayout: function() {
			this.superClass.apply(this, arguments);
		},

		show: function() {
			this.superClass.apply(this, arguments);
			$('#' + this.id).modal('show');
			return this;
		},

		hide: function() {
			this.superClass.apply(this, arguments);
			$('#' + this.id).modal('hide');
			return this;
		},

		add: function(items) {
			var me = this;
			items = $.isArray(items) ? items : [items];
			$.each(items, function(key, item) {
				item.renderTo = me.$el.find('#' + me.id + '-body');
			});
			return this.superClass.apply(this, arguments);
		}
	});

})(window, jQuery, Tn);
