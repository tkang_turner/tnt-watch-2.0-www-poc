(function(window, $, Tn, undefined) {

	/**
	 * @class Tn.Container
	 * A container is a composite of components specified with the property items
	 */
	Tn.define('Tn.Container', {
		extend: 'Tn.Component',
		alias: 'widget.container',

		/**
		 * An array of items containing xtypes or components which will be managed by this container
		 * @cfg {Array}
		 */
		items: undefined,

		dockedItems: undefined,

		constructor: function() {
			var me = this;
			this.superClass.apply(this, arguments);

			// Ensure we have an items array
			this.items = this.items || [];
			this.items = $.isArray(this.items) ? this.items : [this.items];

			// See if this div is being initialied from a content el
			if (me.contentEl) {
				$(me.contentEl).children().each(function() {
					var item = $(this).getAttributes();
					item.text = $(this).text();
					me.items.push(item);
				});
			}

			// Add the tbar items to the docked items
			if (this.tbar) {
				if ($.isArray(this.tbar)) {
					this.tbar = [this.tbar];
				}
				//var pullRight = false;
				$.each(this.tbar, function(index, item) {
					if (typeof item === 'string') {
						switch (item) {
							case '->':
								//pullRight = true;
								break;
						}
						return;
					}
					item.dock = top;
					this.items.push(item);
				});
				delete this.tbar;
			}

			// Add the bbar items to the docked items
			if (this.bbar) {
				if (!$.isArray(this.bbar)) {
					this.bbar = [this.bbar];
				}
				$.each(this.bbar, function(index, item) {
					item.dock = top;
					this.items.push(item);
				});
				delete this.bbar;
			}


			// Build out our items and docked items
			var newItems = [],
				dockedItems = [];
			$.each(this.items, function(index, item) {
				var citem = me.createItem(item);
				if (citem.dock) {
					dockedItems.push(citem);
				}
				else {
					newItems.push(citem);
				}
			});

			this.items = newItems;
			this.dockedItems = dockedItems;
		},

		/**
		 * @private
		 * Creates an item from the input configuration
		 * @param  {Object/Tn.Component} item The item to create
		 * @return {Tn.Component}      The instantiated component
		 */
		createItem: function(item) {
			var me = this;
			if (me.defaults) {
				$.apply(item, me.defaults);
			}

			if (!item.renderTo) {
				item.renderTo = me;
			}
			var citem = me.createWidget(item);
			return citem;
		},

		/**
		 * @event add
		 * Fires when a new component is added to this container
		 */
		/**
		 * Adds a component to this container
		 * @param {Tn.Component} item The last item created
		 */
		add: function(item) {
			var me = this;
			if (!item) {
				return;
			}

			item = $.isArray(item) ? item : [item];
			if (item.length === 0) {
				return;
			}

			$.each(item, function(key, val) {
				me.items.push(me.createItem(val));
				me.fireEvent('add', me.items[me.items.length - 1], me);
			});

			return this.items[this.items.length - 1];
		},

		updateLayout: function() {
			this.superClass.apply(this, arguments);
		},
		
		/**
		 * Removes all items from this container
		 */
		removeAll: function() {
			$.each(this.items, function(key, item) {
				item.destroy();
			});
			this.items = [];
		},

		destroy: function() {
			this.superClass.apply(this, arguments);
			delete this.items;
			delete this.dockedItems;
		}
	});


})(window, jQuery, Tn);
