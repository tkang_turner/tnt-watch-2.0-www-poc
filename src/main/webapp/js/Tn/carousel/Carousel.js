(function(window, $, Tn, undefined) {

    /**
     * @class  Tn.carousel.Carousel
     * A gneric carousel based on Owl Carousel
     */
    Tn.define('Tn.carousel.Carousel', {
        alias: 'widget.carousel',
        extend: 'Tn.Container',
        baseCls: "owl-demo",

        /**
         * If specified, will be overlayed on top of the carousel as a header
         * @cfg {String} showTitle Displays the title of the
         */
        showTitle: undefined,

        /**
         * When displaying the header, we have the option to display a count.
         * The count defaults to the number of items in the carousel, but can be override here to be any value
         * @cfg {Number} countValue The value you want to use to override the header carousel value
         */
        countValue: undefined,

        /**
         * The post fix to use when displaying the count on the carousel header
         * @cfg {String} countText The text that will be displayed for the count value
         */
        countText: 'episodes',

        shakeCls: 'shake',

        /**
         * @private
         * Finalizes the carousel and creates the header if needs be
         */
        finalizeCarousel: function() {
            var me = this;
            me.carousel = me.carousel || {};
            Tn.applyIf(me.carousel, {
                nextCancelled: $.proxy(me.nextCancelled, me),
                prevCancelled: $.proxy(me.prevCancelled, me),
                navigation: true,
                rewindNav: false,
                navigationText: [
                    '<span class="glyphicon glyphicon-chevron-left white"></span>',
                    '<span class="glyphicon glyphicon-chevron-right white"></span>'
                ]
            });
            me.$el.owlCarousel(me.carousel || {});

            // Position the owl header into the div
            if (me.showTitle) {
                me.$el.append(Tn.fm("<div class='owl-header'><h1>{title}</h1><p>{count} {countText}</p></div></div>", {
                    title: me.showTitle,
                    count: me.countValue || (me.items ? me.items.length : 0),
                    countText: me.countText
                }));
            }
        },

        constructor: function() {
            var me = this;
            this.superClass.apply(this, arguments);

            if (!this.items) {
                this.items = [];
            }
            me.finalizeCarousel();
        },

        getDefaultClass: function() {
            return this.superClass.apply(this, arguments) + ' tn-carousel';
        },

        // Make sure to destroy the owl carousel instance
        destroy: function() {
            this.$el.data('owlCarousel').destroy();
            this.superClass.apply(this, arguments);
        },

        shake: function(sel) {
            var me = this;
            $(sel).addClass(me.shakeCls);
            setTimeout(function() {
                $(sel).removeClass(me.shakeCls);
            }, 500);
        },
        prevCancelled: function() {
            this.shake('#' + this.id + ' .owl-prev');
        },
        nextCancelled: function() {
            this.shake('#' + this.id + ' .owl-next');
        }
    });

})(window, jQuery, Tn);
