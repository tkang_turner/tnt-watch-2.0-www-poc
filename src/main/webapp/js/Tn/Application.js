(function(window, $, Tn, undefined) {

	Tn.define('Tn.Dialog', {
		extend: 'Tn.Container',
		alias: 'widget.dialog',
		template: [
			//'<div id="{id}-dialog" tabindex="-1" role="dialog" aria-labelledby="{id}-label" aria-hidden="true">',
			'  <div class="modal-dialog">',
			'    <div class="modal-content">',
			'      <div class="modal-header">',
			'        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">',
			'          <span class="glyphicon glyphicon-remove-sign white"></span>',
			'        </button>',
			'        <h4 class="modal-title" id="{id}-label">{title}</h4>',
			'      </div>',
			'      <div id="{id}-body" class="modal-body">',
			'      </div>',
			'    </div>',
			'  </div>'
			//'</div>'
		].join(''),

		getDefaultClass: function() {
			return this.superClass.apply(this, arguments) + ' modal fade';
		},

		constructor: function(config) {
			var me = this;
			config.id = config.id || Tn.uid();
			config.renderTo = null;

			this.superClass.apply(this, arguments);
			this.$el.attr({
				tabindex: -1,
				role: 'dialog',
				'aria-hidden': true,
				'aria-labelledby': this.id + '-label'
			});

			var tplStr = Tn.fm(this.template, {
				id: this.id,
				title: this.title || ''
			});
			$(tplStr).appendTo(this.$el);

			// Make sure the items get rendered in the modal body
			$.each(this.items || [], function(key, item) {
				item.attachTo(me.$el.find('#' + config.id + '-body'));
			});

			this.$el.appendTo($(document.body), true);

			//$('#' + this.id + '-dialog').modal();
			$('#' + this.id).modal({
				show: false
			});
		},

		updateLayout: function() {
			this.superClass.apply(this, arguments);
		},

		show: function() {
			this.superClass.apply(this, arguments);
			$('#' + this.id).modal('show');
			return this;
		},

		hide: function() {
			this.superClass.apply(this, arguments);
			$('#' + this.id).modal('hide');
			return this;
		},

		add: function(items) {
			var me = this;
			items = $.isArray(items) ? items : [items];
			$.each(items, function(key, item) {
				item.renderTo = me.$el.find('#' + me.id + '-body');
			});
			return this.superClass.apply(this, arguments);
		}
	});

	Tn.Toolbar = Tn.Container.extend({
		alias: 'widget.toolbar',
		constructor: function() {
			this.superClass.apply(this, arguments);
		}
	});

	Tn.Application = Tn.Container.extend({
		alias: 'widget.application',
		renderTo: document.body,

		getDefaultClass: function() {
			return Tn.uiPrefix + 'application';
		},

		constructor: function() {
			this.superClass.apply(this, arguments);
		},

		updateLayout: function() {
			this.superClass.apply(this, arguments);
			this.$el.css({
				"position": 'fixed',
				"left": 0,
				"top": 0,
				"width": "100%",
				"min-height": "100%",
				"height": "auto",
				"margin": "0 auto -40px",
				"padding": "0 0 60px"
			});
		}
	});

	/**
	 * @class Tn.Label
	 * 
	 * Base class for a label
	 * 
	 */
	Tn.define('Tn.Label', {
		extend: 'Tn.Component',
		alias: 'widget.label',
		baseElement: 'p',
		constructor: function(config) {
			config.html = config.text || config.html;
			this.superClass.apply(this, arguments);
		}
	});

	Tn.Page = Tn.Component.extend({
		alias: 'widget.page',
		count: 1,

		getDefaultClass: function() {
			return Tn.uiPrefix + 'page ' + this.superClass.apply(this, arguments);
		},

		constructor: function(config) {
			config = config || {};
			this.superClass.apply(this, arguments);

			var carouselOptions = {
				pagination: false,
				lazyLoad: true,
				items: 3, //10 items above 1000px browser width
				itemsDesktop: [1000, 2], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 1], //2 items between 600 and 0
				itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option						
			};

			for (var i = 0; i < this.count; i++) {
				new Tn.view.carousel({
					renderTo: this,
					el: this,
					carousel: carouselOptions
				});
			}
		}
	});

	Tn.LandingPage = Tn.Page.extend({
		alias: 'widget.landing-page'
	});
	Tn.ShowsPage = Tn.Page.extend({
		alias: 'widget.shows-page',
		count: 5
	});
	Tn.MoviesPage = Tn.Page.extend({
		alias: 'widget.movies-page'
	});
	Tn.LiveTVPage = Tn.Page.extend({
		alias: 'widget.livetv-page'
	});
	Tn.SchedulePage = Tn.Page.extend({
		alias: 'widget.schedule-page'
	});

	Tn.BootStrapApplication = Tn.Application.extend({
		alias: 'widget.bootstrap',
		defaultXType: 'page',
		baseCls: 'tn-application-wrap',

		activeItem: 0,
		setActiveItem: function(page) {
			var me = this,
				curItem = null;
			switch (typeof page) {
				case 'number':
					page = parseInt(page, 10);
					if (page < 0 || page >= me.items.length) {
						return;
					}
					if (me.activeItem !== page && me.activeItem > 0 && me.activeItem < me.items.length) {
						me.items[me.activeItem].hide();
					}
					me.items[page].show();
					me.activeItem = page;
					break;
				case 'string':
					curItem = Tn.getCmp(page);
					break;
				default:
					curItem = page;
			}
			if (!curItem) {
				return;
			}
			$.each(me.items, function(index, item) {
				if (item === curItem) {
					item.show();
					me.activeItem = index;
					return;
				}
				// if (me.activeItem !== page && me.activeItem > 0 && me.activeItem < me.items.length) {
				// 	me.items[me.activeItem].hide();
				// }
				item.hide();
			});
		},

		setBrowseMode: function() {

		},

		constructor: function(config) {
			config = config || {};

			// Hide each item by default
			config.items = config.items || [];
			$.each(config.items, function(index, item) {
				item.hidden = true;
			});

			this.superClass.apply(this, arguments);
			// Add the navbar area
			this.$el.prepend([
				'<div class="navbar navbar-inverse navbar-static-top" role="navigation" id="mainnav">',
				'  <div class="container">',
				'    <div class="navbar-header">',
				'      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">',
				'        <span class="sr-only">Toggle navigation</span>',
				'        <span class="icon-bar"></span>',
				'        <span class="icon-bar"></span>',
				'        <span class="icon-bar"></span>',
				'      </button>',
				'    </div>',
				'    <div class="navbar-collapse collapse no-transition">',
				'      <ul class="nav navbar-nav navbar-right">',
				'      </ul>',
				'    </div>',
				'  </div>',
				'</div>'
			].join(""));

			// Add the footer area
			$('body').append([
				'<div class="tn-footer">',
				'  <ul class="nav nav-pills">',
				'  </ul>',
				'</div>'
			].join(""));

			var me = this,
				navHeader = this.$el.find('.navbar-header'),
				collapseHeader = this.$el.find('.nav.navbar-nav.navbar-right'),
				footer = $('.tn-footer .nav.nav-pills');

			this.navbar = this.navbar || [];
			$.each(this.navbar, function(index, item) {
				if (me.navDefaults) {
					Tn.applyIf(item, me.navDefaults);
				}
				item.renderTo = collapseHeader;
				item.xtype = 'navbutton';
				item = me.createWidget(item);
			});

			this.logo = this.logo || {};
			this.logo.renderTo = navHeader;
			this.logo = Tn.create(this.logo, 'logobutton');

			this.footer = this.footer || [];
			$.each(this.footer, function(index, item) {
				if (me.footerDefaults) {
					Tn.applyIf(item, me.footerDefaults);
				}
				item.renderTo = footer;
				item.xtype = 'navbutton';
				item = me.createWidget(item);
			});

			this.setActiveItem(this.activeItem);
		}
	});


})(window, jQuery, Tn);
