(function(window, $, Tn, undefined) {

	var clickEventType = ((document.ontouchstart!==null)?'click':'touchstart');
	var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
	if (iOS) {
		document.body.addEventListener('touchmove',function(event){
		  event.preventDefault();
		});
	}

	var recentTempImages = [
		"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i246/TBSE1012121300024897_016_640x360.jpg",
		"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i244/TBSE1012121300024901_017_640x360.jpg",
		"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i250/TBSE1012181300024638_009_640x360.jpg",
		"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i248/TBSE1012121300024813_009_640x360.jpg",
		"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i246/TBSE1012121300024812_006_640x360.jpg"
	];
	var mainSampleData = [{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"20:30","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i249/TBSE1012121300024889_009_640x360.jpg","mode":"","title":"The One With Rachel's Inadvertent Kiss","showTitle":"Friends","desc":"Rachel interviews for a great job -- but she may get it for the wrong reason. Monica competes to have a better relationship than Phoebe's.","expiration":"02/06/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"01:25:59","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i250/TBSE1001131400025120_022_640x360.jpg","mode":"","title":"The Time Traveler's Wife","showTitle":"The Time Traveler's Wife","desc":"Despite the fact that Henry's time travels force them apart with no warning, Clare desperately tries to build a life with her one true love.","expiration":"02/11/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"20:59","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i246/TBSE1012121300024897_009_640x360.jpg","mode":"","title":"The Decision: Part Two","showTitle":"Ground Floor","desc":"Brody faces a difficult choice between the job he always wanted with Mansfield and the love he never expected with Jenny. So he tries to come up with a third option. Threepeat is forced to stay with Harvard after losing his apartment, but he soon discovers Harvard's place in a converted funeral home isn't technically zoned for the living.","expiration":"02/14/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"21:00","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i250/TBSE1012181300024638_015_640x360.jpg","mode":"","title":"Hard on Me","showTitle":"Cougar Town","desc":"Andy organizes a Gulf Haven 5k in an effort to save his reputation as Mayor after an embarrassing article comes out in the local paper. Jules uses the race to help her dad, Chick (guest star Ken Jenkins), get out of the house and relive his coaching days.","expiration":"03/05/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"21:01","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i250/TBSE1012181300024677_009_640x360.jpg","mode":"","title":"I Take Thee, Gibbs","showTitle":"Men at Work","desc":"Milo's jealousy is tested when he dates a girl (guest star Emmanuelle Chriqui) who has a \"same-sex ex.\" Gibbs gets carried away with a \"fake\" marriage. Tyler tries to win Myron over with Neal's help. Ryan Phillippe guest stars.","expiration":"03/06/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"42:00","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i251/TBSE1001091400024914_013_640x360.jpg","mode":"","title":"Conan 4529","showTitle":"Conan","desc":"TBS, very funny.","expiration":"02/20/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-PG","duration":"20:56","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i250/TBSE1001091400024848_015_640x360.jpg","mode":"","title":"The Countdown Reflection","showTitle":"The Big Bang Theory","desc":"When Howard and Bernadette decide they want to be married before his NASA space launch, the gang rushes to put on a wedding.","expiration":"02/13/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"20:26","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i250/TBSE1012181300024584_008_640x360.jpg","mode":"","title":"The Curious Case of Jr. Working at the Stool","showTitle":"The Cleveland Show","desc":"Cleveland Jr. takes a job at The Broken Stool, where he infuriates his father by establishing rules; Roberta competes with Lacey, a rich high school student.","expiration":"02/12/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"20:48","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i247/TBSE1012121300024917_015_640x360.jpg","mode":"","title":"New Leads","showTitle":"The Office","desc":"Seeing that Sabre's policy of putting salespeople first has gone to the sales staffs' heads, Michael makes them jump through hoops to get access to a new set of leads sent from corporate. Meanwhile, the sales team looks to make amends to the rest of the office for their recently inflated egos.","expiration":"02/08/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"20:30","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i249/TBSE1001131400024768_015_640x360.jpg","mode":"","title":"Episode 1010","showTitle":"Pete Holmes","desc":"Guests: Rob Corddry","expiration":"02/18/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"41:29","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i249/TBSE1012161300024698_026_640x360.jpg","mode":"","title":"Weird ... Science?","showTitle":"King of the Nerds","desc":"The Nerds discover their war rooms and put their knowledge of science to the test. What will guest judges Bill Nye, Mayim Bialik and Blake Marggraff think of their explosive experiments?","expiration":"02/28/2014"},{"xtype":"movie","isMobile":"mobile","rating":"TV-14","duration":"20:27","thumb":"http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i250/TBSE1001091400024787_015_640x360.jpg","mode":"","title":"Can I Be Frank With You","showTitle":"American Dad","desc":"Francine dresses up in a man suit so she can spend more time with Stan; Steve forms a boy band with his friends.","expiration":"02/12/2014"}];
	var mainSampleDataOffset = -1;

	moment.lang('en', {
	    calendar : {
	        lastDay : '[Yesterday]',
	        sameDay : '[Today]',
	        nextDay : '[Tomorrow]',
	        lastWeek : '[last] dddd',
	        nextWeek : 'dddd',
	        sameElse : 'L'
	    }
	});

	var vidRotate = [
			'adultswim1',
			'cnn1',
			'bones2'
		],
		curVid = 0;

	$.extend(Tn, {
		pageTransitionSpeed: 500
	});

	Tn.episodeInfoDialog = new Tn.Dialog({
		title: "More info goes here",
		items: [
			{
				// html: [
				// 	'<div class="row">',
				// 	'	<div class="col-8 col-sm-8 col-lg-8">',
				// 	'	  <h2>Heading</h2>',
				// 	'	  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>',
				// 	'	</div>',
				// 	'</row>'
				// ].join('')
			}
		]
	});
	Tn.searchResults = new Tn.Container({
		renderTo: 'search-results'
	});
	Tn.recentVids = new Tn.Container({
		renderTo: 'pane-recent'
	});
	Tn.watchList = new Tn.Container({
		renderTo: 'watchlist'/*,
		items: [{
			xtype: 'movie',
			//cls: 'stretch',
			style: 'width:200px;height:120px;margin:5px;display:inline;position:relative;',
			rating: "",
			duration: "",
			thumb: "images/mobcity_mob1_694x390.jpg",
			mode: "",
			title: "",
			desc: "",
			expiration: ""
		},{
			xtype: 'movie',
			//cls: 'stretch',
			style: 'width:200px;height:120px;margin:5px;display:inline;position:relative;',
			rating: "",
			duration: "",
			thumb: "images/mobcity_mob1_694x390.jpg",
			mode: "",
			title: "",
			desc: "",
			expiration: ""
		}]*/
	});

	function parseDuration(val) {
		var min = parseInt(val / 60, 10);
		var sec = parseInt(val - min * 60, 10) + '';
		if (sec.length === 1) {
			sec = '0' + sec;
		}
		return min + "." + sec;
	}

	function resumePlayFromPlayer() {
		$("#browsebut").removeClass("disabled");
		$('.vid-pause-state').fadeOut();
		Tn.Users.removePref(["vidPaused", "vidPosition"]);
		$('#pausebut').show();
		$('#playbut').hide();
		Tn.isPaused = false;
	}

	function resumePlay() {
		Tn.player.resume();
		//Tn.player.play();
		Tn.Video.onContentResume();
		resumePlayFromPlayer();
	}

	function pauseVideo() {
		$('.vid-pause-state').fadeIn();
		$('#pausebut').hide();
		$('#playbut').show();
		Tn.Users.setPref({
			"vidPaused": true,
			"vidPosition": Tn.player.getPlayhead()
		});
		Tn.isPaused = true;
	}

	function updatePlayPosition(position) {
		var cur = parseInt(Tn.player.getPlayhead(), 10);
		var end = parseInt(Tn.player.getDuration(), 10);

		// If a user specified position is specified, then use that instead
		if (position) {
			cur = position;
		}

		if (end < 1) {
			end = 1;
		}
		if (cur > end) {
			cur = end;
		}
		var percent = cur * 100.0 / end;

		if (end > 30 && (end - cur) < 30 /*end * 0.9*/ ) {
			console.error("Endded", end, cur);
			setViewMode("shownext", Tn.pageTransitionSpeed);
		}
		cur = parseDuration(cur);
		end = parseDuration(end);
		$('#playtime').val(Tn.fm("{0} / {1}", cur, end));
		$('#time-slider-but').width(percent + "%");
	}

	$.extend(Tn.Video, {
		buildRecent: function(vids) {
			var output = "";
			Tn.recentVids.removeAll();
			$.each(vids, function(index, vid) {
				Tn.recentVids.add([{
					xtype: 'movie',
					cls: 'stretch',
					style: 'margin: 10px;',
					rating: "",
					duration: vid.duration.value,
					//thumb: vid.thumbnail_url.value,
					thumb: recentTempImages[index % recentTempImages.length],
					mode: "",
					title: vid.headline.value,
					desc: vid.description.value,
					expiration: "",
					minimal: 'playonly'
				}, {
					html:  '<br>'
				}]);
			});
			return output;
		},
		embedContentCb: function(div) {
			Tn.$vidButtons = $("<div id='vid-buttons' class='fade out'><a title='Fullscreen' class='glyphicon glyphicon-fullscreen white'></a><a title='Closed Captions' class='cc white'>cc</a><a title='Facial Recognition' class='glyphicon glyphicon-user white'></a></div>").appendTo(div);
			div.append("<div class='fullscreen loading2'><h1>Loading...</h1></div><div class='fullscreen vid-pause-state'><h1 class='vid-title'></h1><p class='vid-description'></p></div>");
			//div.append("<div class='fullscreen loading2'><h1>Loading...</h1></div>");

			//if (modernizr.touch) {
				$('.loading2').html([
					'<video id="html5video" width=320 height=180 controls>',
					'    <source src="/media/smallvilleclip/smallvilleclip.m3u8">',
					'    Your browser does not support the HTML5 &lt;video&gt; tag.',
					'</video>'
				].join('')).click(function() {
					document.getElementById("html5video").play();
				});
			//}
			setTimeout(function() {
				document.getElementById("html5video").play();
			}, 3000);

			Tn.$vidButtons.find('.glyphicon-fullscreen').on(clickEventType, function() {
				alert("Fullscreen not implemented");
			});

			Tn.$vidButtons.find('.cc').on(clickEventType, function() {
				alert("CC not implemented");
			});

			$('.vid-pause-state').on(clickEventType, function() {
				resumePlay();
			}).dblclick(function() {
				if (Tn.currentMode === 'browse') {
					setViewMode('full', Tn.pageTransitionSpeed);
				}
				else {
					setViewMode('browse', Tn.pageTransitionSpeed);
				}
			});
		},
		onContentPause: pauseVideo,
		onContentPlay: resumePlayFromPlayer,
		onContentEnd: function() {
			$("#browsebut").addClass("disabled");
			Tn.Users.removePref("currentlyPlaying");
			Tn.playerPage.hide();

			$('#pausebut').hide();
			$('#playbut').show();
			Tn.Users.setPref({
				"vidPaused": true,
				"vidPosition": Tn.player.getPlayhead()
			});
			Tn.isPaused = false;
		},
		onPlayTracking: updatePlayPosition
	});

	if (Tn.isMobile) {
		$('#time-slider').hide();
		$('#browsebut').hide();
	}

	function closeMenu() {
		$('#navmain').collapse('hide');
	}

	function showPage(pageId) {
		if (Tn.currentMode === 'full') {
			setViewMode('browse', Tn.pageTransitionSpeed);
		}
		if (Tn.currentPage) {
			if (Tn.currentPage === pageId) {
				return;
			}
			$('#' + Tn.currentPage).hide();
		}
		Tn.currentPage = pageId;
		Tn.Users.setPref("pageId", pageId);
		//console.error("Showing page", pageId)
		$('#' + pageId).show().trigger("pageshow");
		$('#mainnav').find('a').each(function() {
			var pid = $(this).attr('pageId');
			if (!pid) {
				return;
			}
			if (pid !== pageId) {
				$(this).removeClass('active');
			}
			else {
				$(this).addClass('active');
			}
		});
	}

	$('a[pageid], a[data-toggle]').on(clickEventType, function() {
		closeMenu();
		var $this = $(this);
		var pageId = $this.attr('pageId');
		if (!pageId) {
			return;
		}
		showPage(pageId);
		return false;
	});

	$('.toggle-watchlist').on(clickEventType, function() {
		closeMenu();
		if ($('#watchlist').toggle("slow").is(":visible")) {
			console.error("hiding watchlist");
		}
		else {
			console.error("Showing watchlist");
		}
	});

	// Starts monitoring he screen for 500ms
	// Note we use ontimeout, since intervals are bad

	function monitorResize() {
		Tn.resizeEndTime = new Date().getTime() + 500;
		if (!Tn.resizeTimer) {
			doMonitorResize();
		}
	}

	function doMonitorResize() {
		Tn.resizeTimer = null;
		$('#html5video').each(function() {
			$(this).width($('#page-inplayer').width()).height($('#page-inplayer').height());
		});
		Tn.Video.onResize();
		if (new Date().getTime() < Tn.resizeEndTime) {
			Tn.resizeTimer = setTimeout(doMonitorResize, 10);
		}
	}

	function setViewMode(mode, speedInMs) {
		if (!Tn.playerPage) {
			return;
		}
		// If transitioning to a new page, don't do so if our mode is already set
		if (speedInMs > 0 && Tn.mode === mode) {
			return;
		}
		var left = 0,
			top = 0,
			width = 300,
			height = 200,
			padBot = 40 + 20,
			padRight = 20,
			opacity = 0.9,
			winWidth = $(window).width(),
			winHeight = $(window).height(),
			borderWidth = 1,
			browsing = false,
			padding = 0,
			minimized = true;

		if (!mode) {
			mode = Tn.currentMode;
		}

		switch (mode) {
			case 'browse':
				left = winWidth - width - padRight;
				top = winHeight - height - padBot;
				width = 300;
				height = 200;
				borderWidth = 1;
				browsing = true;
				$('#page-related').hide();
				hideSidebar();
				break;

			case 'shownext':
				left = 20;
				top = 80;
				width = 300;
				height = 200;
				borderWidth = 1;
				browsing = true;
				$('#page-related').show();

				// Don't reset the browse mode
				if (Tn.currentMode === mode) {
					return;
				}
				hideSidebar();
				break;

			default:
				left = 0;
				top = 0;
				width = winWidth;
				height = winHeight - 40;
				opacity = 1;
				borderWidth = 0;
				padding = Tn.sideBarShown ? "70px 350px 10px 10px" : "70px 50px 10px 10px";
				minimized = false;
				$('#page-related').hide();
		}

		Tn.currentMode = mode;
		$('#browsebut').toggleClass('active', browsing);
		$('#page-sidebar').toggle(!browsing);

		Tn.Users.setPref("mode", mode);

		if (1 || !speedInMs) {
			Tn.playerPage.css({
				left: left,
				top: top,
				width: width,
				height: height,
				opacity: opacity,
				borderWidth: borderWidth,
				padding: padding
			});
			Tn.playerPage.toggleClass("minimized", minimized);
			monitorResize();
			return;
		}
		Tn.playerPage.animate({
			left: left,
			top: top,
			width: width,
			height: height,
			opacity: opacity,
			borderWidth: borderWidth
		}, {
			progress: function() {
				//console.error("resized", this);
				Tn.Video.onResize();
			},
			duration: speedInMs
		});
	}

	$('.toggle-fullscreen').on(clickEventType, function() {
		if (Tn.currentMode === 'browse') {
			setViewMode('full', Tn.pageTransitionSpeed);
		}
		else {
			setViewMode('browse', Tn.pageTransitionSpeed);
		}
		// $("#page-inplayer").animate({
		//   left: $(window).width() - 300 - 20,
		//   top: $(window).height() - 200 - 40 - 20,
		//   width: 300,
		//   height: 200,
		//   opacity: 0.7
		// }, 500, function() {
		//   // Animation complete.
		// });
		// $('#page-inplayer').css({
		//   left: $(window).width()-200,
		//   right: 20,
		//   top: $(window).height()-200,
		//   bottom: 20,
		// });
		return false;
	});

	function playVideo() {
		$("#html5video").each(function() {
			this.play();
		});
		$('#search').modal('hide');
		if (Tn.isPaused) {
			resumePlay();
			return;
		}

		// if (Tn.isMobile) {
		// 	//document.location.href = 'http://www.youtube.com/watch?v=kv3mLRo8Xaw';
		// 	alert("Video not implemented yet");
		// 	return false;
		// }

		var vid = vidRotate[curVid];
		curVid = (curVid + 1) % vidRotate.length;

		Tn.Users.setPref("currentlyPlaying", vid);

		if (!Tn.playerPage) {
			Tn.playerPage = $('#page-inplayer');

			Tn.Video.createPlayer({
				playerDivId: 'page-inplayer',
				matchPlayerHeight: true,
				autoStart: true,
				contentId: vid
			});
		}
		else {
			try {
				Tn.player.play(vid);
			} catch(e) {
				console.error("Exception:", e.message);
			}
		}
		hideSidebar();
		Tn.playerPage.show();
		setViewMode('full', Tn.pageTransitionSpeed);
		$('.vid-pause-state').hide();
		return false;
	}

	$("#page-inplayer").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
		Tn.Video.onResize();
	});

	function doSearch() {
		delete Tn.searchTimer;
		var val = $('#search-button').val();
		Tn.searchResults.removeAll();
		if (val.length === 0) {
			return;
		}
		Tn.searchResults.add([{
			xtype: 'label',
			text: Tn.fm("Results for '{0}'", val)
		}, {
			xtype: 'carousel',
			numItems: 3,
			hideTitle: true,
			items: [
				$.extend({}, mainSampleData[0]),
				$.extend({}, mainSampleData[1]),
				$.extend({}, mainSampleData[2])
			],
			carousel: {
				pagination: false,
				//lazyLoad: true,
				items: 4, //10 items above 1000px browser width
				itemsDesktop: [1000, 4], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 4], //2 items between 600 and 0
				itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option		
			}
		}, {
			xtype: 'label',
			text: "Season 2"
		}, {
			xtype: 'carousel',
			numItems: 3,
			hideTitle: true,
			items: [
				$.extend({}, mainSampleData[3]),
				$.extend({}, mainSampleData[4]),
				$.extend({}, mainSampleData[6]),
				$.extend({}, mainSampleData[7]),
				$.extend({}, mainSampleData[8]),
				$.extend({}, mainSampleData[9])
			],
			carousel: {
				pagination: false,
				//lazyLoad: true,
				items: 4, //10 items above 1000px browser width
				itemsDesktop: [1000, 4], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 4], //2 items between 600 and 0
				itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option		
			}
		}, {
			xtype: 'label',
			text: "Season 3"
		}, {
			xtype: 'carousel',
			numItems: 3,
			hideTitle: true,
			items: [
				$.extend({}, mainSampleData[10]),
				$.extend({}, mainSampleData[11]),
				$.extend({}, mainSampleData[12])
			],
			carousel: {
				pagination: false,
				//lazyLoad: true,
				items: 4, //10 items above 1000px browser width
				itemsDesktop: [1000, 4], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 4], //2 items between 600 and 0
				itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option		
			}
		}]);
	}

	$('#search-button').keyup(function() {
		if (Tn.searchTimer) {
			clearTimeout(Tn.searchTimer);
		}
		Tn.searchTimer = setTimeout(doSearch, 500);
	});

	$("#search").on('show.bs.modal', function() {
		console.error("Search shown");
		setTimeout(function() {
			$('#search-button').focus().blur().focus().select();
		}, 1000);
	});

	$('#playbut').on(clickEventType,playVideo);
	$('#pausebut').on(clickEventType,function() {
		if (!Tn.player) {
			return;
		}
		Tn.player.pause();
	});
	$('#time-slider').on(clickEventType,function(e) {
		if (!Tn.player) {
			return;
		}
		var pos = e.pageX - $(this).offset().left;
		var fullWidth = $('#time-slider').width();
		if (pos > fullWidth) {
			pos = fullWidth;
		}
		var vidPos = pos * Tn.player.getDuration() / fullWidth;
		Tn.player.seek(vidPos);
		updatePlayPosition(vidPos);
	});

	$('#live-east,#live-west').on(clickEventType,playVideo);

	function showSidebar() {
		if (Tn.sideBarShown) {
			return;
		}
		Tn.sideBarShown = true;
		$('.sidebar').css('right', 0);
	}

	function hideSidebar() {
		if (!Tn.sideBarShown) {
			return;
		}
		Tn.sideBarShown = false;

		var sb = $('#page-sidebar');
		sb.css('right', -260);
		sb.find('.nav a').removeClass("active");
		sb.find('.pane').hide();
	}
	$('.sidebar .nav a').on(clickEventType,function() {
		var $this = $(this);
		if ($this.hasClass("active")) {
			hideSidebar();
			setViewMode();
			return;
		}
		showSidebar();
		setViewMode();
		$('.sidebar .nav a').removeClass("active");
		$('.sidebar .pane').hide();
		$this.addClass("active");
		var paneId = $(this).attr("paneId");
		$("#" + paneId).show();
	});

	function doResize() {
		var w = $(window).width() - 450;
		if (w < 20) {
			w = 20;
		}
		$('#time-slider').width(w + "px");
		//$('#page-inplayer').find(".pane").height($(window).height()-120));

		setViewMode();
	}
	$(window).resize(doResize);

	// Setup our tiled map movie view only when the page is shown
	var images = [
		'images/ciw13_694x390.jpg',
		'images/mobcity_mob1_694x390.jpg',
		'images/t1_mc02_694x390.jpg',
		'images/bf_694x390.jpg',
		'images/tbs-homepage-feature-trustme-07-705x405_120320130318.jpg',
		'images/GF-TBS-TheGift-705x405_112720131237.jpg',
		'images/tbs-features-pete-holmes-tux-705x405_120320130331.jpg',
		'images/tbs-features-conan-14-705x405_083020131249.jpg'
	];

	var owl = $(".owl-demo");
	var owlmain = $(".owl-main");
	var owllive = $(".owl-live");
	var owlwatch = $(".owl-watch");
	var icnt = 0;

	owl.owlCarousel({
		beforeInit: function(el) {
			for (var i = 0; i < 5; i++) {
				el.append('<div class="item"><img class="lazyOwl" src="' + images[(icnt++) % images.length] + '"></div>');
			}
		},
		pagination: false,
		//lazyLoad: true,
		items: 3, //10 items above 1000px browser width
		itemsDesktop: [1000, 2], //5 items between 1000px and 901px
		itemsDesktopSmall: [900, 2], // betweem 900px and 601px
		itemsTablet: [600, 1], //2 items between 600 and 0
		itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
	});

	owl.each(function() {
		$(this).append("<div class='owl-header'><h1>Mob City</h1><p>Updated: 11.30.2013</p><p>Seasons: 1, 6 episodes</p></div></div>");
	});

	Tn.create({
		xtype: 'seriescollection',
		renderTo: 'page-shows',
		url: 'api/getCollection.php',
		listeners: {
			click: function() {
				playVideo();
			},
			watch: function() {
				$('#watchlist').show('slow');
			},
			info: function() {
				$('#infopage').modal('show');
				//Tn.episodeInfoDialog.show();
			}
		}
	});

	Tn.create({
		xtype: 'schedulecollection',
		renderTo: 'page-schedule',
		url: 'api/info.php',
		cls: 'schedule',
		listeners: {
			click: function() {
				playVideo();
			},
			watch: function() {
				$('#watchlist').show('slow');
			},
			info: function() {
				$('#infopage').modal('show');
				//Tn.episodeInfoDialog.show();
			}
		}
	});

	Tn.create({
		xtype: 'carousel',
		numItems: 3,
		renderTo: 'page-movies',
		items: [
			$.extend({}, mainSampleData[0]),
			$.extend({}, mainSampleData[1]),
			$.extend({}, mainSampleData[2])
		],
		carousel: {
			pagination: false,
			//lazyLoad: true,
			items: 2, //10 items above 1000px browser width
			itemsDesktop: [1000, 2], //5 items between 1000px and 901px
			itemsDesktopSmall: [900, 2], // betweem 900px and 601px
			itemsTablet: [600, 1], //2 items between 600 and 0
			itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option		
		}
	});
	Tn.create({
		xtype: 'carousel',
		numItems: 3,
		renderTo: 'page-movies',
		items: [
			$.extend({}, mainSampleData[3]),
			$.extend({}, mainSampleData[4]),
			$.extend({}, mainSampleData[5])
		],
		carousel: {
			pagination: false,
			//lazyLoad: true,
			items: 2, //10 items above 1000px browser width
			itemsDesktop: [1000, 2], //5 items between 1000px and 901px
			itemsDesktopSmall: [900, 2], // betweem 900px and 601px
			itemsTablet: [600, 1], //2 items between 600 and 0
			itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option		
		}
	});

/*	Tn.create({
		xtype: 'seriescollection',
		renderTo: 'page-shows2',
		url: 'api/getCollection.php',
		minimal: true,
		listeners: {
			click: function() {
				playVideo();
			},
			watch: function() {
				$('#watchlist').show('slow');
			},
			info: function() {
				$('#infopage').modal('show');
				//Tn.episodeInfoDialog.show();
			}
		}
	});*/

	/*	for (var i = 0; i < 6; i++) {
		new Tn.view.carousel({
			url: 'api/proxy.php?id=430119',
			renderTo: 'page-shows',
			el: this,
			carousel: {
				pagination: false,
				//lazyLoad: true,
				items: 3, //10 items above 1000px browser width
				itemsDesktop: [1000, 2], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 1], //2 items between 600 and 0
				itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option		
			}
		});
	}
*/
	owlwatch.owlCarousel({
		beforeInit: function(el) {
			for (var i = 0; i < 5; i++) {
				el.append('<div class="item"><img src="' + images[(icnt++) % images.length] + '"></div>');
			}

		},
		pagination: false,
		//lazyLoad: true,
		items: 10, //10 items above 1000px browser width
		itemsDesktop: [1000, 5], //5 items between 1000px and 901px
		itemsDesktopSmall: [900, 3], // betweem 900px and 601px
		itemsTablet: [600, 2], //2 items between 600 and 0
		itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
	});

	owlmain.owlCarousel({
		beforeInit: function(el) {
			for (var i = 0; i < 5; i++) {
				//el.append('<div class="item" style="height:550px;background:transparent;margin: 10px;"><div class="maintitle"><div class="bottom"><h1>Cougar Town</h1><p>What\'s New</p></div></div><div class="mainimage"><div class="playbut"></div></div><div class="mainsubimg"></div><div class="mainsubimg"></div></div>');
				var id = Tn.uid();
				mainSampleDataOffset = (mainSampleDataOffset+1) % mainSampleData.length;
				el.append(Tn.applyTpl([
					'<div class="item" id="{id}" style="height:650px;background:transparent;margin: 10px;">',
					'    <div class="maintitle">',
					'        <div class="bottom">',
					'            <h1>{showTitle}</h1>',
					'            <p>What\'s New</p>',
					'        </div>',
					'    </div>',
					'    <div class="mainimage" id="{id}-movie">',
					//'        <div class="playbut"></div>',
					'    </div>',
					'    <div class="mainsubimg" id="{id}-sub"></div>',
					// '    <div class="mainsubimg" id="{id}-sub2"></div>',
					'</div>'], {
					id: id,
					showTitle: mainSampleData[mainSampleDataOffset].showTitle,
					title: mainSampleData[mainSampleDataOffset].title
				}));
				Tn.create(Tn.apply({
					xtype: 'movie',
					renderTo: id + '-movie',
					minimal: true,
					cls: 'stretch',
					style: 'width:100%;height:100%;'
				}, mainSampleData[mainSampleDataOffset]));
				mainSampleDataOffset = (mainSampleDataOffset+1) % mainSampleData.length;
				Tn.create(Tn.apply({
					xtype: 'movie',
					renderTo: id + '-sub',
					minimal: 'playonly',
					cls: 'stretch mainsub1',
					style: 'width:50%;height:100%;'
				}, mainSampleData[mainSampleDataOffset]));
				mainSampleDataOffset = (mainSampleDataOffset+1) % mainSampleData.length;
				Tn.create(Tn.apply({
					xtype: 'movie',
					renderTo: id + '-sub',
					minimal: 'playonly',
					cls: 'stretch mainsub2',
					style: 'width:50%;height:100%;'
				}, mainSampleData[mainSampleDataOffset]));

/*				Tn.create({
					xtype: 'movie',
					renderTo: id + '-sub',
					minimal: 'playonly',
					rating: "",
					duration: "",
					cls: 'stretch mainsub1',
					style: 'width:50%;height:100%;',
					thumb: "http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i247/TBSE1001101400024969_016_640x360.jpg",
					mode: "",
					title: "Men at work",
					desc: "",
					expiration: ""
				});
				Tn.create({
					xtype: 'movie',
					renderTo: id + '-sub',
					minimal: 'playonly',
					rating: "",
					duration: "",
					cls: 'stretch mainsub2',
					style: 'width:50%;height:100%;',
					thumb: "http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i250/TBSE1012191300024735_015_640x360.jpg",
					mode: "",
					title: "My name is Earl",
					desc: "",
					expiration: ""
				});*/
			}
		},
		pagination: true,
		//lazyLoad : true,
		//itemsScaleUp:true,
		items: 4, //10 items above 1000px browser width
		itemsDesktop: [1400, 3], //5 items between 1000px and 901px
		itemsDesktopSmall: [1100, 2], // betweem 900px and 601px
		itemsTablet: [600, 1], //2 items between 600 and 0
		itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
	});

	owllive.owlCarousel({
		beforeInit: function(el) {
			el.append('<div class="item" style="height:550px;background:transparent;margin: 10px;"><div class="livetitle"><div class="bottom"><h1>East Coast</h1></div></div><div id="liveimage1"></div></div>');
			el.append('<div class="item" style="height:550px;background:transparent;margin: 10px;"><div class="livetitle"><div class="bottom"><h1>West Coast</h1></div></div><div id="liveimage2"></div></div>');

			Tn.create({
				xtype: 'movie',
				renderTo: 'liveimage1',
				rating: "",
				duration: "",
				cls: 'stretch',
				thumb: "http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i245/fp-default-640x360_031420121238.jpg",
				mode: "",
				title: "Fresh prince of Bel Air",
				desc: "A black rapper from a tough neighborhood found himself in a cartoonish sitcom in this funky comedy. Will was the kid from West Philadelphia who was sent West to live with wealthy relatives in Bel Air, California, when things got a little too dangerous back in the 'hood.",
				expiration: ""
			});
			Tn.create({
				xtype: 'movie',
				renderTo: 'liveimage2',
				rating: "",
				duration: "",
				cls: 'stretch',
				thumb: "http://i.cdn.turner.com/v5cache/TBS/Images/Dynamic/i245/mnie-default-640x360_031420121243.jpg",
				mode: "",
				title: "My name is Earl",
				desc: "Earl decides that one way he can balance the karmic scales is by quitting smoking, but this proves to be more difficult than he imagined.",
				expiration: ""
			});
		},
		pagination: true,
		//lazyLoad : true,
		//itemsScaleUp:true,
		items: 2, //10 items above 1000px browser width
		itemsDesktop: [1000, 2], //5 items between 1000px and 901px
		itemsDesktopSmall: [900, 2], // betweem 900px and 601px
		itemsTablet: [600, 1], //2 items between 600 and 0
		itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
	});

	showPage(Tn.Users.getPref('pageId', 'page-landing'));
	doResize();

	(function() {

		Tn.Users.initialize({
			commentId: 'gigya-comments',
			commentCategory: 'test',
			onStartup: function() {

				var currentlyPlaying = Tn.Users.getPref('currentlyPlaying');
				var mode = Tn.Users.getPref('mode');
				var paused = Tn.Users.getPref("vidPaused", false);
				var position = Tn.Users.getPref("vidPosition", undefined) || 0;
				var attempts = 0;

				showPage(Tn.Users.getPref('pageId', 'page-landing'));

				function attemptSeek() {
					try {
						if (Tn.player.getDuration()<5) {
							throw "empty";
						}
						Tn.player.seek(position);
						Tn.player.pause();
						updatePlayPosition(position);
						console.error("position", position);
						if (mode !== Tn.currentMode && mode !== "shownext") {
							setViewMode(mode, 0);
						}
					} catch(e) {
						attempts++;
						console.error("Failed seek attempts", attempts);
						if (attempts<30) {
							setTimeout(function() {
								attemptSeek();
							}, 1000);
						}
					}
				}

				if (currentlyPlaying) {
					playVideo(currentlyPlaying);
					if (paused) {
						attemptSeek();
					}
				}

				doResize();
			}
		});
	})();

	//$('#search2').modal();

})(window, jQuery, Tn);
